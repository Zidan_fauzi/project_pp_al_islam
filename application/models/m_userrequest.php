<?php

class M_userrequest extends CI_Model{
    private $_table = "tb_santri";
    
    public function getAll(){
        $this->db->select("
        $this->_table.id_santri,
        $this->_table.nama,
        $this->_table.gambar_profile,
        $this->_table.alamat,
        $this->_table.tempat_lahir,
        $this->_table.tanggal_lahir,
        santri_request.created_at,
        santri_request.updated_at,
        santri_request.status");

        $this->db->join('santri_request', "$this->_table.id_santri = santri_request.id_santri AND santri_request.is_delete = 0", 'RIGHT OUTER');
        $this->db->where("tb_santri.is_delete = 0");
        $this->db->order_by("created_at DESC");
       return $this->db->get($this->_table)->result_array();
    }

    public function getById($id){
        $this->db->select("
        $this->_table.id_santri,
        $this->_table.nama,
        $this->_table.gambar,
        $this->_table.email,
        $this->_table.level,
        santri_request.created_at,
        santri_request.updated_at,
        santri_request.status");

        $this->db->join('santri_request', "$this->_table.id_santri = santri_request.id_santri AND santri_request.is_delete = 0", 'RIGHT OUTER');
        return $this->db->get_where($this->_table, ["tb_santri.id_santri" => $id])->row();
    }

    public function confirm($id){
        unset($this->id_santri);

        $data = array(   
            'status' => '1',
            'updated_at' => date("y-m-d H:i:s")
        );
        $this->db->update('santri_request', $data, array('id_santri' => $id));
    }

    public function update_userAccept($id){
        unset($this->id_santri);

        $data = array(   
            'status' => '1',
            'updated_at' => date("y-m-d H:i:s")
        );
        $this->db->update('tb_santri', $data, array('id_santri' => $id));
    }

    public function reject($id){
        unset($this->id_santri);

        $data = array(   
            'status' => '2',
            'updated_at' => date("y-m-d H:i:s")
        );
        $this->db->update('santri_request', $data, array('id_santri' => $id));
        $this->db->update('tb_santri', $data, array('id_santri' => $id));
    }
}

?>