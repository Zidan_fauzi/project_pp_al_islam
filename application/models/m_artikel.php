<?php

class M_artikel extends CI_Model{
    private $_table = "tb_artikel";
    private $_table2 = "tb_kategori";
    
    public $id_artikel;
    public $nama_artikel;
    public $created_at;
    public $slug;
    public $updated_at;
    public $gambar;
    public $deskripsi;
    public $is_delete;

    public function getAll()
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0");
        $this->db->order_by("$this->_table.updated_at DESC");
        $this->db->group_by("$this->_table.id_artikel");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }
    
    public function get_kategori()
    {
        $this->db->select('*');
        $this->db->from('tb_kategori');
        $this->db->where('tb_kategori.is_delete = 0');
        $query = $this->db->get();
        return $query->result();
    }
    
	public function jumlah_data()
    {
        $this->db->where("tb_artikel.is_delete = 0");
        return $this->db->get("tb_artikel")->num_rows();
    }    
    
    public function getById($id)
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.id_kategori,
        $this->_table.created_at,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        return $this->db->get_where($this->_table, ["tb_artikel.id_artikel" => $id])->row();
    }

    public function getByIdDelete($id)
    {
        $this->db->select("$this->_table.nama_artikel");
        return $this->db->get_where($this->_table, ["tb_artikel.id_artikel" => $id])->row();
    }

    public function uploadArticle($data) 
    {
        return $this->db->insert('tb_artikel', $data);
    }

    public function updateArticle($id, $file_name)
    {
        unset($this->id_artikel);        
        $post = $this->input->post();

        if(!empty($file_name)){
            $this->gambar = $file_name;
        }else{
            unset($this->gambar);
        }
  
        $this->nama_artikel = $post["namaArtikel"];
        $this->id_kategori = $post["kategori"];
        $this->updated_at = date("y-m-d H:i:s");
        $this->slug = $post["slug"];;
        unset($this->created_at);
        $this->deskripsi = $post["deskripsi"];
        
        $this->db->update($this->_table, $this, array('id_artikel' => $id));
    }
    
    public function delete($id)
    {
        $q="UPDATE tb_artikel SET tb_artikel.is_delete = '1', tb_artikel.id_kategori = '0' WHERE id_artikel = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function restore($id)
    {
        $q="UPDATE tb_artikel SET tb_artikel.is_delete = '0' WHERE id_artikel = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function getArtikel($id)
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 AND tb_artikel.id_artikel = ".$id);

        $query = $this->db->get($this->_table)->row();
        return $query;
    }

    public function latestArticle()
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.id_admin,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.is_featured,
        $this->_table.is_recomended,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama,
        GROUP_CONCAT(tb_tags.tags) as tags");

        $this->db->join('artikel_tags', "$this->_table.id_artikel = artikel_tags.id_artikel AND artikel_tags.is_delete = 0", 'LEFT OUTER');
        $this->db->join('tb_tags', "artikel_tags.id_tags = tb_tags.id_tags", 'LEFT OUTER');
        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 AND tb_artikel.id_admin =".$this->session->userdata('id_admin'));
        $this->db->order_by("$this->_table.created_at DESC");
        $this->db->group_by("$this->_table.id_artikel");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }

    public function latest_article3()
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.id_admin,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama ");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 ");
        $this->db->limit(3);
        $this->db->order_by("$this->_table.created_at DESC");
        $this->db->group_by("$this->_table.id_artikel");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }

    public function latest_article_all()
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.id_admin,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama ");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 ");
        $this->db->order_by("$this->_table.created_at DESC");
        $this->db->group_by("$this->_table.id_artikel");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }

    public function oldest_article_all()
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.id_admin,
        $this->_table.slug,
        $this->_table.updated_at,
        
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama ");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 ");
        $this->db->order_by("$this->_table.created_at ASC");
        $this->db->group_by("$this->_table.id_artikel");
        $this->db->limit(12);

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }

    public function fetch_data($limit, $start)
    {
        $this->db->select(" 
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.created_at,
        tb_artikel.slug,
        tb_artikel.updated_at,
        tb_artikel.deskripsi,
        tb_artikel.gambar,
        tb_kategori.kategori,
        tb_admin.nama");

        $this->db->from("tb_artikel");
        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0");
        $this->db->order_by("id_artikel", "DESC");
        $this->db->group_by("tb_artikel.id_artikel");
        
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function fetch_jumlah_data()
    {
        $this->db->where("tb_artikel.is_delete = 0");
        return $this->db->get("tb_artikel")->num_rows();;
    }

    public function artikel_rekomended()
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.id_admin,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama ");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 ");
        $this->db->limit(3);
        $this->db->order_by("$this->_table.id_artikel DESC");
        $this->db->group_by("$this->_table.id_artikel");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }

    public function detail($slug)
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.slug,
        $this->_table.updated_at,
        $this->_table.deskripsi,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama,
        tb_admin.gambar as gambar_profile");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 AND tb_artikel.slug = '$slug'");
        $this->db->order_by("$this->_table.updated_at DESC");
        $this->db->group_by("$this->_table.id_artikel");

        $query = $this->db->get($this->_table)->row_array();
        return $query;
    }

    public function article_related($kategori)
    {
        $this->db->select(" 
        $this->_table.id_artikel,
        $this->_table.nama_artikel,
        $this->_table.created_at,
        $this->_table.slug,
        $this->_table.gambar,
        tb_kategori.kategori,
        tb_admin.nama");

        $this->db->join('tb_kategori', "$this->_table.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "$this->_table.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 AND tb_kategori.kategori = '$kategori'");
        $this->db->order_by("$this->_table.updated_at DESC");
        $this->db->group_by("$this->_table.id_artikel");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }
  }		
