<?php

class M_ustadz extends CI_Model{
    private $_table = "tb_ustadz";
    
    public $id_artikel;
    public $nama_artikel;
    public $created_at;
    public $slug;
    public $updated_at;
    public $gambar;
    public $deskripsi;
    public $is_delete;

    public function getAll()
    {
        $this->db->select("*");

        $this->db->where("$this->_table.is_delete = 0");
        $this->db->order_by("$this->_table.created_at DESC");
        $this->db->group_by("$this->_table.id_ustadz");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }

    public function uploadUstadz($data) 
    {
        return $this->db->insert('tb_ustadz', $data);
    }

    public function updateUstadz($data, $id) 
    {
        $this->db->where('tb_ustadz.id_ustadz = '.$id);
        return $this->db->update('tb_ustadz', $data);
    }

    public function getAllById($id){
        $this->db->select("*");
        $this->db->where("tb_ustadz.is_delete = 0 AND tb_ustadz.id_ustadz = ".$id);
        $this->db->order_by("tb_ustadz.created_at DESC");
       return $this->db->get("tb_ustadz")->row();
    }

    public function getByIdDelete($id)
    {
        $this->db->select("$this->_table.nama_ustadz");
        return $this->db->get_where($this->_table, ["$this->_table.id_ustadz" => $id])->row();
    }
    
    public function delete($id)
    {
        $q="UPDATE tb_ustadz SET tb_ustadz.is_delete = '1' WHERE id_ustadz = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function restore($id)
    {
        $q="UPDATE tb_ustadz SET tb_ustadz.is_delete = '0' WHERE id_ustadz = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function Check($email)
    {
        $this->db->select("*");
        return $this->db->get_where("tb_admin", ["tb_admin.email" => $email, "tb_admin.is_delete" => "0"])->row();
    }
    
}		
