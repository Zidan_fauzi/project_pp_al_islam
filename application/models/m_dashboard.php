<?php

class M_dashboard extends CI_Model{
    private $_table = "tb_admin";
    
    public $id_user;
    public $nama;
    public $created_at;
    public $updated_at;
    public $email;
    public $level;
    public $is_delete;

    function jumlah_data(){
        $this->db->where("tb_admin.is_delete = 0");
        return $this->db->get("tb_admin")->num_rows();
    }

    public function getUser(){
        $this->db->select("
        $this->_table.id_admin,
        $this->_table.level,
        $this->_table.nama,
        $this->_table.email,
        $this->_table.gambar");
        $this->db->where("tb_admin.is_delete = 0 AND tb_admin.id_admin = ".$this->session->userdata("id_admin"));
       return $this->db->get($this->_table)->row_array();
    }

    public function count_santri(){
        $this->db->where("tb_santri.is_delete = 0 AND tb_santri.status = 1");
        $query = $this->db->get('tb_santri'); 
        return $query->result();
    }

    public function count_santri_daftar(){
        $this->db->where("santri_request.is_delete = 0");
        $query = $this->db->get('santri_request'); 
        return $query->result();
    }

    public function get_santriAll(){
        $this->db->select("
            tb_santri.nama,
            tb_santri.jenis_kelamin,
            tb_santri.tempat_lahir,
            tb_santri.tanggal_lahir,
            tb_santri.alamat,
            tb_santri.nama_orangtua,
            tb_santri.no_handphone,
            tb_santri.gambar_profile,
            tb_santri.status,
            santri_request.created_at,
            santri_request.updated_at
        ");
        $this->db->join("tb_santri", "tb_santri.id_santri = santri_request.id_santri", "LEFT OUTER");
        $this->db->where("tb_santri.is_delete = 0");
        $query = $this->db->get('santri_request'); 
        return $query->result_array();
    }

    public function count_artikel(){
        $this->db->where("tb_artikel.is_delete = 0");
        $query = $this->db->get("tb_artikel"); 
        return $query->result();
    }

    public function count_ustadz(){
        $this->db->where("tb_ustadz.is_delete = 0");
        $query = $this->db->get("tb_ustadz"); 
        return $query->result();
    }

    public function profil_edit($id, $data)
    {
        $this->db->where("tb_admin.id_admin = ".$id);
        $this->db->update("tb_admin", $data);
    }
    public function delete_akun($id)
    {   
        $data = array(
            'tb_admin.is_delete' => 1
        );
        $this->db->where("tb_admin.id_admin = ".$id);
        $this->db->update("tb_admin", $data);
    }
}