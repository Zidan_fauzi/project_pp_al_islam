<?php

class M_home extends CI_Model{
    private $_table = "tb_artikel";

    function head(){
        $this->db->select("tb_artikel.gambar, tb_artikel.nama_artikel, tb_artikel.slug, tb_kategori.kategori, tb_artikel.created_at, tb_artikel.deskripsi ");
        $this->db->from("tb_artikel");
        $this->db->join("tb_kategori","tb_kategori.id_kategori = tb_artikel.id_kategori");
        $this->db->where("tb_artikel.is_delete = 0");
        $this->db->limit(6);
        $this->db->order_by("tb_artikel.id_artikel","DESC");
        return $this->db->get()->result();
    }

    function sel_ustadz(){
        $this->db->select("tb_ustadz.foto_ustadz, tb_ustadz.nama_ustadz, tb_ustadz.quotes ");
        $this->db->from("tb_ustadz");
        $this->db->where("tb_ustadz.is_delete = 0");
        $this->db->order_by("tb_ustadz.id_ustadz","DESC");
        return $this->db->get()->result();
    }

    
}