<?php

class M_admin extends CI_Model{
    private $_table = "tb_admin";
    private $_table2 = "tb_kategori";
    
    public $id_artikel;
    public $nama_artikel;
    public $created_at;
    public $slug;
    public $updated_at;
    public $gambar;
    public $deskripsi;
    public $is_delete;

    public function getAll()
    {
        $this->db->select(" 
        $this->_table.id_admin,
        $this->_table.email,
        $this->_table.nama,
        $this->_table.gambar,
        $this->_table.level,
        $this->_table.updated_at,
        $this->_table.created_at,
        $this->_table.gambar");

        $this->db->where("$this->_table.is_delete = 0 AND $this->_table.level = 1");
        $this->db->order_by("$this->_table.updated_at DESC");
        $this->db->group_by("$this->_table.id_admin");

        $query = $this->db->get($this->_table)->result_array();
        return $query;
    }

    public function getByIdDelete($id)
    {
        $this->db->select("$this->_table.nama");
        return $this->db->get_where($this->_table, ["$this->_table.id_admin" => $id])->row();
    }
    
    public function delete($id)
    {
        $q="UPDATE tb_admin SET tb_admin.is_delete = '1' WHERE id_admin = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function restore($id)
    {
        $q="UPDATE tb_admin SET tb_admin.is_delete = '0' WHERE id_admin = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function Check($email)
    {
        $this->db->select("*");
        return $this->db->get_where("tb_admin", ["tb_admin.email" => $email, "tb_admin.is_delete" => "0"])->row();
    }
    
}		
