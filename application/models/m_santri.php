<?php

class M_santri extends CI_Model{
    
    public function getAll(){
        $this->db->select("*");
        $this->db->where("tb_santri.is_delete = 0 AND tb_santri.status = 1");
        $this->db->order_by("tb_santri.created_at DESC");
       return $this->db->get("tb_santri")->result_array();
    }

    public function uploadSantri($data) {
        return $this->db->insert('tb_santri', $data);
    }

    public function insertRequest($data) {
        return $this->db->insert('santri_request', $data);
    }

    public function updateSantri($data, $id) {
        $this->db->where('tb_santri.id_santri = '.$id);
        return $this->db->update('tb_santri', $data);
    }

    public function updateRequsetSantri($data, $id) {
        $this->db->where('santri_request.id_santri = '.$id);
        return $this->db->update('santri_request', $data);
    }

    public function deleteSantri($data, $id) {
        $this->db->where('tb_santri.id_santri = '.$id);
        return $this->db->update('tb_santri', $data);
    }

    public function getAllById($id){
        $this->db->select("*");
        $this->db->where("tb_santri.is_delete = 0 AND tb_santri.id_santri = ".$id);
        $this->db->order_by("tb_santri.created_at DESC");
       return $this->db->get("tb_santri")->row();
    }

    public function daftarAdmin($data)
    {
        $this->db->insert("tb_admin",$data);
    }
}

?>