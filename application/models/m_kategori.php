<?php

class M_kategori extends CI_Model{
    private $_table = "tb_kategori";
    
    public $id_kategori;
    public $kategori;
    public $created_at;
    public $updated_at;
    public $slug;
    public $is_delete;

    function jumlah_data(){        
        $this->db->where('tb_kategori.is_delete = 0');
        return $this->db->get($this->_table)->num_rows();;
    }
    
    public function getAll(){
        $this->db->select("
        $this->_table.id_kategori,
        $this->_table.kategori,
        $this->_table.slug,
        $this->_table.created_at,
        $this->_table.updated_at");
        $this->db->where("tb_kategori.is_delete = 0");
        $this->db->order_by("tb_kategori.id_kategori ASC");
       return $this->db->get($this->_table)->result_array();
    }
    
    public function getById($id){
        $this->db->select("
        $this->_table.id_kategori,
        $this->_table.kategori,
        $this->_table.slug,
        $this->_table.created_at,
        $this->_table.updated_at ");
        return $this->db->get_where($this->_table, ["id_kategori" => $id])->row();
    }

    public function edit($id){
        unset($this->id_kategori);        
        $post = $this->input->post();

        $this->kategori = $post["kategori"];
        $this->slug = $post["slug"];
        $this->updated_at = date("y-m-d H:i:s");
        unset($this->created_at);
        $this->db->update($this->_table, $this, array('id_kategori' => $id));
    }

    public function delete($id){       
        $q="UPDATE tb_kategori SET tb_kategori.is_delete = '1' WHERE id_kategori = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function deleteArtikelByKategori($id){       
        $q="UPDATE tb_artikel SET tb_artikel.is_delete = '1' WHERE tb_artikel.id_kategori = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function restore($id){       
        $q="UPDATE tb_kategori SET tb_kategori.is_delete = '0' WHERE id_kategori = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function restoreKategoriById($id){       
        $q="UPDATE tb_artikel SET tb_artikel.is_delete = '0' WHERE tb_artikel.id_kategori = $id";
        $query=$this->db->query($q);
        return $query;
    }

    public function get_kategori_keyword($keyword){
        $this->db->select("
        $this->_table.id_kategori,
        $this->_table.kategori,
        $this->_table.slug,
        $this->_table.created_at,
        $this->_table.updated_at");
        $this->db->where("tb_kategori.is_delete = 0 AND (id_kategori LIKE '%$keyword%' OR kategori LIKE '%$keyword%')");
        
        $query = $this->db->get($this->_table)->result();
        return $query;
        
    }

    public function getjumlah($keyword){
        $this->db->select("
        $this->_table.id_kategori,
        $this->_table.kategori,
        $this->_table.slug,
        $this->_table.created_at,
        $this->_table.updated_at");
        $this->db->where("tb_kategori.is_delete = 0 AND (id_kategori LIKE '%$keyword%' OR kategori LIKE '%$keyword%')");
        
        $q = $this->db->get($this->_table)->num_rows();
        return $q;
    }

    public function upload() {
        unset($this->id_kategori);        
        $post = $this->input->post();

        $this->kategori = $post["kategori"];
        $this->slug = $post["slug"];
        $this->created_at = date("y-m-d H:i:s");
        $this->updated_at = date("y-m-d H:i:s");
        $this->is_delete = '0';
		$this->db->insert($this->_table, $this);
    }
}
?>