<?php

class M_main extends CI_Model{

public function getDataCate2($dataid)
{		
    $this->db->select("
    tb_kategori.id_kategori,
    tb_kategori.kategori,
    tb_kategori.gambar,
    tb_kategori.slug,
    tb_artikel.id_artikel,
    tb_artikel.nama_artikel,
    tb_artikel.id_kategori,
    tb_artikel.deskripsi,
    tb_artikel.gambar,
    tb_artikel.created_at,
    tb_artikel.updated_at,
    tb_artikel.slug as slugs,
    tb_artikel.view_ammount,
    tb_artikel.likes_amount,
    tb_admin.gambar as fotoprof,  
    tb_admin.nama");

    $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori AND tb_kategori.is_delete = 0", 'LEFT OUTER');
    $this->db->join('tb_admin', "tb_artikel.id_admin= tb_admin.id_admin", 'LEFT OUTER');
    $this->db->where("tb_kategori.id_kategori = '$dataid' AND tb_artikel.is_delete = 0");

    $Cate2 = $this->db->get('tb_artikel')->result();

    $x = count($Cate2);

    if ($x>0) {
      for ($i=0; $i < $x ; $i++) { 
        $desc[$i] = strip_tags($Cate2[$i]->deskripsi);

        $str     = $desc[$i];
        $order   = array("\r\n", "\n", "\r", "\t");
        $replace = '';
        $newstr = str_replace($order, $replace, $str);

        $response['Cate2'][$i] = array(
            'id_artikel' => $Cate2[$i]->id_artikel,
            'nama_artikel' => $Cate2[$i]->nama_artikel,
            'gambar' => $Cate2[$i]->gambar,
            'created_at' => $Cate2[$i]->created_at,
            'slug' => $Cate2[$i]->slug,
            'updated_at' => $Cate2[$i]->updated_at,
            'deskripsi' =>  $newstr,
            'kategori' => $Cate2[$i]->kategori,
            'author' => $Cate2[$i]->nama,
            'id_kategori' => $Cate2[$i]->id_kategori,
            'view_ammount' => $Cate2[$i]->view_ammount,
            'likes_amount' => $Cate2[$i]->likes_amount,
            'foto_profile' => $Cate2[$i]->fotoprof,
            'msg' => 'mpus'
        );
      }
    }else{
      $response['Cate2'][0] = array('msg' => "nope", );
    } 
    return $response;
  }

    public function get_artikelLimit_keyword($keyword){
        $this->db->select(" 
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.created_at,
        tb_artikel.slug as slugs,
        tb_artikel.updated_at,
        tb_artikel.is_featured,
        tb_artikel.is_recomended,
        tb_artikel.deskripsi,
        tb_artikel.gambar,
        tb_artikel.likes_amount,
        tb_artikel.view_ammount,
        tb_kategori.id_kategori,
        tb_kategori.kategori,
        tb_kategori.slug,
        tb_admin.nama,
        tb_admin.gambar as fotoprof,     
        GROUP_CONCAT(tb_tags.tags) as tags");

        $this->db->join('artikel_tags', "tb_artikel.id_artikel = artikel_tags.id_artikel AND artikel_tags.is_delete = 0", 'LEFT OUTER');
        $this->db->join('tb_tags', "artikel_tags.id_tags = tb_tags.id_tags", 'LEFT OUTER');
        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        // $this->db->like('kategori = '.$keyword);
        $this->db->where("nama_artikel LIKE '%$keyword%' AND tb_artikel.is_delete = 0");
        $this->db->order_by("tb_artikel.id_artikel ASC");
        $this->db->group_by("tb_artikel.id_artikel");

        $search = $this->db->get('tb_artikel')->result_array();

        $x = count($search);

        if ($keyword==null) {
          $response['msg'] = "404 Not Found";
          return $response['msg'];
           
        }else{
        if ($x > 0) {
    		 for ($i=0; $i < $x ; $i++) { 
    		   $desc[$i] = strip_tags($search[$i]["deskripsi"]);

               $str     = $desc[$i];
               $order   = array("\r\n", "\n", "\r", "\t");
               $replace = '';
               $newstr = str_replace($order, $replace, $str);

    		   $response['search'][$i] = array(
               'id_artikel' => $search[$i]["id_artikel"],
    		       'nama_artikel' => $search[$i]["nama_artikel"],
    		       'gambar' => $search[$i]["gambar"],
    		       'created_at' => $search[$i]["created_at"],
    		       'slug' => $search[$i]["slug"],
    		       'updated_at' => $search[$i]["updated_at"],
    		       'deskripsi' =>  $newstr,
               'id_kategori' => $search[$i]["id_kategori"],
    		       'kategori' => $search[$i]["kategori"],
    		       'author' => $search[$i]["nama"],
               'view_ammount' => $search[$i]["view_ammount"],
    		       'likes_amount' => $search[$i]["likes_amount"],
               'foto_profile' => $search[$i]["fotoprof"],
    		   );
    		}
            $response['msg'] = "Data Found";
            $response['jumlah_artikel'] = $x;
    		return $response;
        }else{
            $response['msg'] = "404 Not Found";
            return $response['msg'];
        }
      }
    }

    public function getBlog($id)
	{
		$this->db->select(" 
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.id_kategori,
        tb_artikel.created_at,
        tb_artikel.is_featured,
        tb_artikel.updated_at,
        tb_artikel.deskripsi,
        tb_artikel.gambar,
        tb_artikel.likes_amount,
        tb_artikel.view_ammount,
        tb_kategori.kategori,
        tb_admin.nama,
        artikel_tags.id_tags,
        tb_tags.tags,
        tb_tags.slug");

        $this->db->join('artikel_tags', "tb_artikel.id_artikel = artikel_tags.id_artikel ", 'LEFT OUTER');
        $this->db->join('tb_tags', "artikel_tags.id_tags = tb_tags.id_tags", 'LEFT OUTER');
        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->group_by("tb_tags.id_tags");
        return $this->db->get_where("tb_artikel", ["tb_artikel.id_artikel" => $id])->result_array();
	}
	
  public function getPopular()
  {
        $this->db->select("
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.id_kategori,
        tb_artikel.deskripsi,
        tb_artikel.gambar,
        tb_artikel.created_at,
        tb_artikel.updated_at,
        tb_artikel.slug as slugs,
        tb_artikel.likes_amount,
        tb_artikel.view_ammount,
        tb_kategori.id_kategori,
        tb_kategori.kategori,
        tb_kategori.gambar,
        tb_kategori.slug,    
        tb_admin.nama");

        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori AND tb_kategori.is_delete = 0", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel.id_admin= tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0");
        $this->db->order_by('tb_artikel.view_ammount DESC');
        $this->db->limit(5);
        $query = $this->db->get('tb_artikel');
        return $query->result_array();
  }

  public function getLatest()
  {
        $this->db->select("
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.id_kategori,
        tb_artikel.deskripsi,
        tb_artikel.gambar,
        tb_artikel.created_at,
        tb_artikel.updated_at,
        tb_artikel.slug as slugs,
        tb_artikel.likes_amount,
        tb_artikel.view_ammount,
        tb_kategori.id_kategori,
        tb_kategori.kategori,
        tb_kategori.gambar,
        tb_kategori.slug,    
        tb_admin.nama");

        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori AND tb_kategori.is_delete = 0", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel.id_admin= tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0");
        $this->db->limit(5);
        $this->db->order_by('tb_artikel.created_at DESC');
       	return $this->db->get('tb_artikel')->result_array();

  }

  public function getRecommend()
  {
        $this->db->select("
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.id_kategori,
        tb_artikel.deskripsi,
        tb_artikel.gambar,
        tb_artikel.created_at,
        tb_artikel.updated_at,
        tb_artikel.slug as slugs,
        tb_artikel.view_ammount,
        tb_artikel.likes_amount,
        tb_kategori.id_kategori,
        tb_kategori.kategori,
        tb_kategori.gambar,
        tb_kategori.slug,    
        tb_admin.nama");

        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori AND tb_kategori.is_delete = 0", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel.id_admin= tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel.is_delete = 0 AND tb_artikel.is_recomended = 1");
        $query = $this->db->get('tb_artikel');
        return $query->result_array();  

  }

  public function getdesription($id)
  {
        $this->db->select("
        tb_artikel.slug,
        tb_artikel.deskripsi");

        $this->db->where("tb_artikel.id_artikel = $id");
        return $this->db->get("tb_artikel")->result();
  }

  public function getrelatedcat($namekat)
    {
        $this->db->select("
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.deskripsi,
        tb_artikel.id_kategori,
        tb_artikel.gambar,
        tb_artikel.created_at,
        tb_artikel.updated_at,
        tb_artikel.slug as slugs,
        tb_artikel.view_ammount,
        tb_artikel.likes_amount,
        tb_kategori.kategori,
        tb_kategori.slug,
        tb_admin.gambar as fotoprof,  
        tb_admin.nama");

        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori ", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel.id_admin= tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_kategori.kategori ='$namekat'");
        $this->db->order_by("id_artikel DESC");
        $this->db->limit(4);

        $related = $this->db->get('tb_artikel')->result();

        $x = count($related);
        if ($x>0) {
          for ($i=0; $i < $x ; $i++) { 
            $desc[$i] = strip_tags($related[$i]->deskripsi);

            $str     = $desc[$i];
            $order   = array("\r\n", "\n", "\r", "\t");
            $replace = '';
            $newstr = str_replace($order, $replace, $str);

            $createdate = date("j F Y", strtotime($related[$i]->created_at));
            $update = date("j F Y", strtotime($related[$i]->updated_at));

            $response['related'][$i] = array(
                'id_artikel' => $related[$i]->id_artikel,
                'nama_artikel' => $related[$i]->nama_artikel,
                'gambar' => $related[$i]->gambar,
                'created_at' => $createdate,
                'slug' => $related[$i]->slug,
                'updated_at' => $update,
                'deskripsi' =>  $newstr,
                'kategori' => $related[$i]->kategori,
                'id_kategori' => $related[$i]->id_kategori,
                'author' => $related[$i]->nama,
                'view_ammount' => $related[$i]->view_ammount,
                'likes_amount' => $related[$i]->likes_amount,
                'foto_profile' => $related[$i]->fotoprof,
                'msg' => 'mpus'
            );
        }
      }else{
        $response['related']['msg'] = "nope";
      } 
      return $response;
    }

    public function totalLike($id)
    {
        $this->db->select('*');
        $this->db->where("tb_artikel_likes.id_artikel = $id");
        return $this->db->get("tb_artikel_likes")->result_array();
    }

    public function registerAc($email,$nama,$encrypt2,$gambar,$created_at,$updated_at)
    {
        $data = array(
            'email' => $email, 
            'nama' => $nama,
            'password' => $encrypt2,
            'gambar' => $gambar,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0',
            'level' => '2'
         );

        $regis = $this->db->insert('tb_admin', $data);

        if ($regis) {
            $msg = "Register Success";
            return $msg;
        }else{
            $msg = "Register failed";
            return $msg;
        }
    }

    public function checkingEmail($email)
    {
        $query = "SELECT * FROM tb_admin WHERE email = '$email'";
        return $query;

        // $this->db->select('*');
        // $this->db->from("tb_admin");
        // $this->db->where("email = '$email'");
        // $query = $this->db->get();
        // if ($query->num_rows() == 0) {
        //     return FALSE;
        // } else {
        //     return $query->result();
        // }
    }

    public function updateAc($id,$nama,$gambar)
    {
       $data = array(
            'nama' => $nama,
            'gambar' => $gambar 
       );
      $this->db->where("id_admin = $id");
      $update = $this->db->update('tb_admin', $data);

        if ($update) {
            $msg = "Update success";
            return $msg;
        }else{

        }
    }

    public function updateAcN($id,$nama)
    {
       $data = array(
            'nama' => $nama
       );
      $this->db->where("id_admin = $id");
      $update = $this->db->update('tb_admin', $data);

        if ($update) {
            $msg = "Update success";
            return $msg;
        }else{

        }
    }

    public function checkPassword($id)
    {
        $this->db->select('*');
        $this->db->where("tb_admin.id_admin = $id");
        return $this->db->get('tb_admin')->result();
    }

    public function checkUser($id)
    {
        $this->db->select('*');
        $this->db->where("tb_admin.id_admin = $id");
        $user = $this->db->get('tb_admin')->result_array();

        if ($user[0]["level"] == 0) {
            $status = "admin";
        }else if($user[0]["level"] == 1){
            $status = "author";
        }else{
            $status = "user";
        }

        $response = array(
          'id_admin' => $user[0]["id_admin"],
          'email' => $user[0]["email"],
          'nama' => $user[0]["nama"],
          'status' => $status,
          'gambar' => $user[0]["gambar"],
          'cookie' => $user[0]["cookie"],
          'msg' => "Edit Profile Success"
        );

        return $response;
    }

    public function checkUsers($email)
    {
        $this->db->select('*');
        $this->db->where("tb_admin.email = '$email'");
        return $this->db->get('tb_admin')->result();
    }

    public function checkUser2($id)
    {
        $this->db->select('*');
        $this->db->where("tb_admin.id_admin = $id");
        $user = $this->db->get('tb_admin')->result_array();

        if ($user[0]["level"] == 0) {
            $status = "admin";
        }else if($user[0]["level"] == 1){
            $status = "author";
        }else{
            $status = "user";
        }
        $response['empus'] = array(
          'id_admin' => $user[0]["id_admin"],
          'email' => $user[0]["email"],
          'nama' => $user[0]["nama"],
          'gambar' => $user[0]["gambar"],
          'status' => $status,
          'cookie' => $user[0]["cookie"]
          );

        return $response;
    }

    public function changePassword($id,$encrypt4)
    {
       $data = array(
            'password' => $encrypt4
       );

       $this->db->where("id_admin = $id");
       $updatePass =$this->db->update('tb_admin', $data);

       if ($updatePass) {
          $msg['message'] = "successfully change password";
          return $msg;
       }else{
          $msg['message'] = "sorry something wrong with server";
          return $msg;
       }
    }

    public function feedbackWI($id_admin,$email,$feed,$gambar,$created_at,$updated_at)
    {
        $data = array(
            'id_admin' => $id_admin, 
            'email' => $email,
            'feed' => $feed,
            'image' => $gambar,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $feed = $this->db->insert('tb_feedback', $data);

        if ($feed) {
            $msg = "giving feedback success";
            return $msg;
        }else{
            $msg = "feedback failed to send";
            return $msg;
        }   
    }

    public function feedbackNM($id_admin,$email,$feed,$created_at,$updated_at)
    {
        $data = array(
            'id_admin' => $id_admin, 
            'email' => $email,
            'feed' => $feed,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $feed = $this->db->insert('tb_feedback', $data);

        if ($feed) {
            $msg = "giving feedback success";
            return $msg;
        }else{
            $msg = "feedback failed to send";
            return $msg;
        }   
    }
    
    public function update_reset_key($email, $reset_key){
        $this->db->where('email', $email);
        $data = array('reset_password' => $reset_key);
        $this->db->update('tb_admin', $data);
        // exit($this->db->last_query());
        if($this->db->affected_rows()>0){
          return TRUE;
        }else{
          return FALSE;
        }
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function index_komentar($id_artikel){
        $this->db->select(" 
        tb_komentar.id_komentar,
        tb_komentar.komentar,
        tb_komentar.created_at,
        tb_komentar.updated_at,
        tb_komentar.likes_komentar_ammount,
        tb_artikel.slug,
        tb_admin.nama,
        tb_admin.gambar");

        $this->db->join('tb_artikel', "tb_komentar.id_artikel = tb_artikel.id_artikel", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_komentar.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        // $this->db->join('tb_komentar_likes', "tb_komentar_likes.id_komentar = tb_komentar.id_komentar", 'LEFT OUTER');
        $this->db->where("tb_komentar.is_delete = 0 AND tb_artikel.id_artikel = '$id_artikel'");
        $this->db->order_by('created_at ASC');

        $query = $this->db->get('tb_komentar')->result_array();

        // var_dump($this->db->last_query());exit();

        $x = count($query);
        for ($i=0; $i < $x ; $i++) { 

          $createdate = $this->time_elapsed_string($query[$i]['created_at']);
          $update = $this->time_elapsed_string($query[$i]['updated_at']);

          $response[$i] = array(
              'id_komentar' => $query[$i]["id_komentar"],
              'komentar' => $query[$i]["komentar"],
              'created_at' => $createdate,
              'updated_at' => $update,
              'slug' => $query[$i]["slug"],
              'likes_komentar_ammount' => $query[$i]["likes_komentar_ammount"],
              'nama' => $query[$i]["nama"],
              'gambar' => $query[$i]["gambar"]
          );
        }

        return $response;

    }

    public function check_komen($id_admin,$id_komentar)
    {
        $this->db->select(" 
        tb_komentar_likes.status,
        tb_komentar_likes.id_admin,
        tb_komentar_likes.id_komentar_like
        ");

        $this->db->where("tb_komentar_likes.id_komentar='$id_komentar' AND tb_komentar_likes.id_admin='$id_admin'");

        $query = $this->db->get('tb_komentar_likes')->result_array();

        $response['meong'] = array(
              'status' => $query[0]["status"],
              'id_admin' => $query[0]["id_admin"],
              'id_komentar_like' => $query[0]["id_komentar_like"],
          );

        return $response;
    }

    public function check_reply($id_admin,$id_komentar_reply)
    {
        $this->db->select(" 
        tb_komentar_likes_reply.status,
        tb_komentar_likes_reply.id_admin,
        tb_komentar_likes_reply.id_komentar_like_reply
        ");

        $this->db->where("tb_komentar_likes_reply.id_komentar_reply='$id_komentar_reply' AND tb_komentar_likes_reply.id_admin='$id_admin'");

        $query = $this->db->get('tb_komentar_likes_reply')->result_array();

        $response['meong'] = array(
              'status' => $query[0]["status"],
              'id_admin' => $query[0]["id_admin"],
              'id_komentar_like_reply' => $query[0]["id_komentar_like_reply"],
          );

        return $response;
    }

    public function index_komentarLimit($id_artikel){
        $this->db->select(" 
        tb_komentar.id_komentar,
        tb_komentar.komentar,
        tb_komentar.created_at,
        tb_komentar.updated_at,
        tb_artikel.slug,
        tb_admin.nama,
        tb_admin.gambar");

        $this->db->join('tb_artikel', "tb_komentar.id_artikel = tb_artikel.id_artikel", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_komentar.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_komentar.is_delete = 0 AND tb_artikel.id_artikel = '$id_artikel'");
        $this->db->order_by('created_at ASC');
        $this->db->limit(2);

        $query = $this->db->get('tb_komentar')->result();
        return $query;
    }

    public function index_komentar_reply($id){
        $this->db->select("
        tb_komentar_reply.id_komentar_reply, 
        tb_komentar_reply.id_komentar,
        tb_komentar_reply.komentar,
        tb_komentar_reply.created_at,
        tb_komentar_reply.updated_at,
        tb_komentar_reply.likes_reply_ammount,
        tb_komentar.id_artikel,
        tb_artikel.slug,
        tb_admin.nama,
        tb_admin.gambar");

        $this->db->join('tb_komentar', "tb_komentar_reply.id_komentar = tb_komentar.id_komentar", 'LEFT OUTER');
        $this->db->join('tb_artikel', "tb_komentar.id_artikel = tb_artikel.id_artikel", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_komentar_reply.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        // $this->db->join('tb_komentar_likes_reply', "tb_komentar_likes_reply.id_komentar_reply = tb_komentar_reply.id_komentar_reply", 'LEFT OUTER');
        $this->db->where("tb_komentar_reply.is_delete = 0 AND tb_komentar_reply.id_komentar = '$id'");
        $this->db->order_by('created_at ASC');

        $query = $this->db->get('tb_komentar_reply')->result_array();

        $x = count($query);
        for ($i=0; $i < $x ; $i++) { 

          $createdate = $this->time_elapsed_string($query[$i]['created_at']);
          $update = $this->time_elapsed_string($query[$i]['updated_at']);

          $response[$i] = array(
              'id_komentar_reply' => $query[$i]["id_komentar_reply"],
              'id_komentar' => $query[$i]["id_komentar"],
              'komentar' => $query[$i]["komentar"],
              'created_at' => $createdate,
              'updated_at' => $update,
              'id_artikel' => $query[$i]["id_artikel"],
              'slug' => $query[$i]["slug"],
              'likes_reply_ammount' => $query[$i]["likes_reply_ammount"],
              'nama' => $query[$i]["nama"],
              'gambar' => $query[$i]["gambar"]
          );
        }

        return $response;
    }

    public function LikeArtikel($id_artikel,$id_admin,$created_at,$updated_at)
    {
        $data = array(
            'id_artikel' => $id_artikel,
            'id_admin' => $id_admin, 
            'status' => '1',
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $like_artikel = $this->db->insert('tb_artikel_likes', $data);

        if ($like_artikel) {
            $msg = "Likes";
            return $msg;
        }else{
            $msg = "Hated";
            return $msg;
        }   
    }

    public function LikeArtikel2($updated_at)
    {
        $data = array(
            'status' => '1',
            'updated_at' => $updated_at
         );

        $like_artikel = $this->db->update('tb_artikel_likes', $data);

        if ($like_artikel) {
            $msg = "Likes";
            return $msg;
        }else{
            $msg = "Hated";
            return $msg;
        }   
    }

    public function ListLike($id_artikel){
        $this->db->select(" 
        tb_artikel_likes.id_like,
        tb_artikel_likes.id_admin,
        tb_artikel_likes.status,
        tb_artikel.slug,
        tb_admin.nama,
        tb_admin.gambar");

        $this->db->join('tb_artikel', "tb_artikel_likes.id_artikel = tb_artikel.id_artikel", 'LEFT OUTER');
        $this->db->join('tb_admin', "tb_artikel_likes.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("tb_artikel_likes.is_delete = 0 AND tb_artikel.id_artikel = '$id_artikel'");
        $this->db->order_by('tb_artikel_likes.created_at DESC');

        $query = $this->db->get('tb_artikel_likes')->result();
        return $query;
    }

    public function DislikeArtikel($id_like,$updated_at)
    {
        $data = array(
            'status' => '0',
            'updated_at' => $updated_at
         );

        $this->db->where("id_like = $id_like");
        $update = $this->db->update('tb_artikel_likes', $data);

        if ($update) {
            $msg = "You have dislike something";
            return $msg;
        }else{
            $msg = "sorry but you can't dislike this";
            return $msg;
        }
    }

    public function insertKomen($id_artikel,$id_admin,$komentar,$created_at,$updated_at)
    {
        $data = array(
            'id_artikel' => $id_artikel,
            'id_admin' => $id_admin, 
            'komentar' => $komentar,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $komen = $this->db->insert('tb_komentar', $data);

        if ($komen) {
            $msg = "Successfully comment something";
            return $msg;
        }else{
            $msg = "You should try our comment feature";
            return $msg;
        }   
    }

    public function insertBalas($id_komentar,$id_admin,$komentar,$created_at,$updated_at)
    {
        $data = array(
            'id_komentar' => $id_komentar,
            'id_admin' => $id_admin, 
            'komentar' => $komentar,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $komen = $this->db->insert('tb_komentar_reply', $data);

        if ($komen) {
            $msg = "Successfully Reply something";
            return $msg;
        }else{
            $msg = "You should try our reply comment feature";
            return $msg;
        }   
    }

    public function checkLikeArtikel2($id,$id_admin)
    {
        $this->db->select('*');
        $this->db->where("tb_artikel_likes.id_artikel = $id AND tb_artikel_likes.id_admin = $id_admin");
        return $this->db->get('tb_artikel_likes')->result_array();
    }

    public function checkBookmark2($id,$id_admin)
    {
        $this->db->select('*');
        $this->db->where("artikel_bookmarks.id_artikel = $id AND artikel_bookmarks.id_admin = $id_admin");
        return $this->db->get('artikel_bookmarks')->result_array();
    }

     public function checkLikeArtikel()
    {
        $this->db->select('*');
        return $this->db->get('tb_artikel_likes')->result_array();
    }

    public function checkLikeKomen2($id_komentar,$id_admin)
    {
        $this->db->select('*');
        $this->db->where("tb_komentar_likes.id_komentar = $id_komentar AND tb_komentar_likes.id_admin = $id_admin");
        return $this->db->get('tb_komentar_likes')->result_array();
    }

    public function checkLikeKomen()
    {
        $this->db->select('*');
        return $this->db->get('tb_komentar_likes')->result_array();
    }

    public function checkLikeReply()
    {
        $this->db->select('*');
        return $this->db->get('tb_komentar_likes_reply')->result_array();
    }

    public function checkStatus($id,$id_admin)
    {
        $this->db->select('*');
        $this->db->where("tb_artikel_likes.id_artikel = $id AND tb_artikel_likes.id_admin = $id_admin");
        return $this->db->get('tb_artikel_likes')->result_array();
    }

    public function LikeKomentar($id_komentar,$id_admin,$created_at,$updated_at)
    {
        $data = array(
            'id_komentar' => $id_komentar,
            'id_admin' => $id_admin, 
            'status' => '1',
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $like_artikel = $this->db->insert('tb_komentar_likes', $data);

        if ($like_artikel) {
            $msg = "Likes";
            return $msg;
        }else{
            $msg = "Hated";
            return $msg;
        }   
    }

    public function LikeKomentar2($updated_at)
    {
        $data = array(
            'status' => '1',
            'updated_at' => $updated_at
         );

        $like_artikel = $this->db->update('tb_komentar_likes', $data);

        if ($like_artikel) {
            $msg = "Likes";
            return $msg;
        }else{
            $msg = "Hated";
            return $msg;
        }   
    }

    public function DislikeKomentar($id_komentar_like,$updated_at)
    {
        $data = array(
            'status' => '0',
            'updated_at' => $updated_at
         );

        $this->db->where("id_komentar_like = $id_komentar_like");
        $update = $this->db->update('tb_komentar_likes', $data);

        if ($update) {
            $msg = "You have dislike something";
            return $msg;
        }else{
            $msg = "sorry but you can't dislike this";
            return $msg;
        }
    }

    public function LikeReply($id_komentar_reply,$id_admin,$created_at,$updated_at)
    {
        $data = array(
            'id_komentar_reply' => $id_komentar_reply,
            'id_admin' => $id_admin, 
            'status' => '1',
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $like_artikel = $this->db->insert('tb_komentar_likes_reply', $data);

        if ($like_artikel) {
            $msg = "Likes";
            return $msg;
        }else{
            $msg = "Hated";
            return $msg;
        }   
    }

    public function LikeReply2($updated_at)
    {
        $data = array(
            'status' => '1',
            'updated_at' => $updated_at
         );

        $like_artikel = $this->db->update('tb_komentar_likes_reply', $data);

        if ($like_artikel) {
            $msg = "Likes";
            return $msg;
        }else{
            $msg = "Hated";
            return $msg;
        }   
    }

    public function DislikeReply($id_like_reply,$updated_at)
    {
        $data = array(
            'status' => '0',
            'updated_at' => $updated_at
         );

        $this->db->where("id_komentar_like_reply = $id_like_reply");
        $update = $this->db->update('tb_komentar_likes_reply', $data);

        if ($update) {
            $msg = "You have dislike something";
            return $msg;
        }else{
            $msg = "sorry but you can't dislike this";
            return $msg;
        }
    }

    public function Bookmark($id_admin,$id_artikel,$created_at,$updated_at)
    {
        $data = array(
            'id_admin' => $id_admin,
            'id_artikel' => $id_artikel, 
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $book = $this->db->insert('artikel_bookmarks', $data);

        if ($book) {
            $msg = "Added";
            return $msg;
        }else{
            $msg = "idc";
            return $msg;
        }   
    }

    public function Bookmark2($updated_at)
    {
        $data = array(
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $book = $this->db->update('artikel_bookmarks', $data);

        if ($book) {
            $msg = "Added";
            return $msg;
        }else{
            $msg = "idc";
            return $msg;
        }   
    }

    public function removeBook($id_bookmark,$updated_at)
    {
        $data = array(
            'updated_at' => $updated_at,
            'is_delete' => '1'
         );

        $this->db->where("id_bookmarks = $id_bookmark");
        $update = $this->db->update('artikel_bookmarks', $data);

        if ($update) {
            $msg = "You have remove bookmark";
            return $msg;
        }else{
            $msg = "sorry but you can't remove this this";
            return $msg;
        }
    }

    public function checkBookmark()
    {
        $this->db->select('*');
        return $this->db->get('artikel_bookmarks')->result_array();
    }

    public function display_bookmark($id_admin)
    {
        $this->db->select(" 
        tb_artikel.id_artikel,
        tb_artikel.nama_artikel,
        tb_artikel.id_kategori,
        tb_artikel.deskripsi,
        tb_artikel.gambar,
        tb_artikel.created_at,
        tb_artikel.updated_at,
        tb_artikel.slug as slugs,
        tb_artikel.view_ammount,
        tb_artikel.likes_amount,
        tb_kategori.kategori,
        tb_kategori.slug, 
        tb_admin.nama,
        tb_admin.gambar as fotoprof");

        $this->db->join('tb_artikel', "artikel_bookmarks.id_artikel = tb_artikel.id_artikel", 'LEFT OUTER');
        $this->db->join('tb_kategori', "tb_artikel.id_kategori = tb_kategori.id_kategori AND tb_kategori.is_delete = 0", 'LEFT OUTER');
        $this->db->join('tb_admin', "artikel_bookmarks.id_admin = tb_admin.id_admin", 'LEFT OUTER');
        $this->db->where("artikel_bookmarks.is_delete = 0 AND artikel_bookmarks.id_admin = '$id_admin'");
        $this->db->order_by('created_at DESC');

        $query = $this->db->get('artikel_bookmarks');
        // $query = $this->db->query("SELECT * FROM tb_artikel WHERE is_delete=0 AND is_recomended=1 LIMIT 4");
        $bookmark = $query->result_array();  

        $x = count($bookmark);
        for ($i=0; $i < $x ; $i++) { 
          $desc[$i] = strip_tags($bookmark[$i]["deskripsi"]);

          $str     = $desc[$i];
          $order   = array("\r\n", "\n", "\r", "\t");
          $replace = '';
          $newstr = str_replace($order, $replace, $str);

          $createdate = date("j F Y", strtotime($bookmark[$i]['created_at']));
          $update = date("j F Y", strtotime($bookmark[$i]['updated_at']));

          $response['bookmark'][$i] = array(
              'id_artikel' => $bookmark[$i]["id_artikel"],
              'nama_artikel' => $bookmark[$i]["nama_artikel"],
              'gambar' => $bookmark[$i]["gambar"],
              'created_at' => $createdate,
              'slug' => $bookmark[$i]["slug"],
              'updated_at' => $update,
              'deskripsi' =>  $newstr,
              'kategori' => $bookmark[$i]["kategori"],
              'author' => $bookmark[$i]["nama"],
              'view_ammount' => $bookmark[$i]["view_ammount"],
              'likes_amount' => $bookmark[$i]["likes_amount"],
              'foto_profile' => $bookmark[$i]["fotoprof"]
          );
        }
        return $response;
    }

    public function getToken()
    {
        $this->db->select("*");
        $this->db->where('tb_token.is_delete = 0');
        $query = $this->db->get('tb_token');
        $querysql = $query->result_array();

        return $querysql;  
    }

    public function saveToken($token,$created_at,$updated_at)
    {
        $data = array(
            'device_token' => $token,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0'
         );

        $insertT = $this->db->insert('tb_token', $data);

        if($insertT){
            $msg = 'Record Successfully Inserted Into MySQL Database.';
            return $msg;
        }
        else{
            var_dump($Sql_Query);
        }
    }

    public function saveNotif($title,$newstr,$file_name)
    {
        $created_at = date('y-m-d H:i:s');
        $updated_at = date('y-m-d H:i:s');

        $data = array(
            'title' => $title,
            'body' => $newstr,
            'image' => $file_name,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'is_delete' => '0',
        );

        $insertN = $this->db->insert('tb_notif', $data);

        if($insertN){
            $msg = 'Record Successfully Inserted Into MySQL Database.';
            return $msg;
        }
        else{
            var_dump($Sql_Query);
        }
        
    }

    public function getNotif()
    {
        $this->db->select("*");
        $this->db->where('tb_notif.is_delete = 0');
        $this->db->order_by('tb_notif.created_at DESC');
        $querysql = $this->db->get('tb_notif')->result_array();
        return $querysql;
    }


}
?>