<?php

class M_login extends CI_Model{
    private $_table3 = "tb_admin";
    private $_table2 = "tb_art";
    
    public $id_admin;
    public $email;
    public $nama;
    public $password;
    public $level;
    
    function logged_id(){
        return $this->session->userdata('level');
    }

    function check_login($_table2, $data){
        $this->db->select('*');
        $this->db->from($_table2);
        $this->db->where($data);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    function logged_in($id){
        unset($this->id_admin);        
        $post = $this->input->post();
        $data = array(
            'updated_at' => date("y-m-d H:i:s")
        );
        $this->db->update($this->_table3, $data, array('id_admin' => $id));
    }

    function logout_at($id){
        unset($this->id_admin);        
        $post = $this->input->post();
        $data = array(
            'cookie' => '',
            'updated_at' => date("y-m-d H:i:s")
        );
        $this->db->update('tb_admin', $data, array('id_admin' => $id));
    }
    
    function register($file_name){
        if(!empty($file_name)){
            $this->gambar = $file_name;
        }else{
            unset($this->gambar);
        }
        $pengacak = "p3ng4c4k";
        $password1 = MD5($this->input->post('password'));
        $password2 = md5($pengacak . md5($password1));

        $this->email = $this->input->post('email'); 
        $this->nama = $this->input->post('nama');
        $this->password = $password2; 
        $this->created_at = date("y-m-d H:i:s");
        $this->updated_at = date("y-m-d H:i:s");
        $this->is_delete = '0';
        $this->level = '2';

        $this->db->insert('tb_admin', $this);
    }

    function request($id){
        unset($this->id_admin);        
        $post = $this->input->post();
        $data = array(
            'id_admin' => $id,
            'status' => '2',
            'created_at' => date("y-m-d H:i:s"),
            'updated_at' => date("y-m-d H:i:s"),
            'is_delete' => '0'
        );
        $this->db->insert('user_request', $data);
    }

    function get_by_cookie($cookie){
        $this->db->select('*');
        $this->db->from('tb_admin');
        $this->db->where('cookie', $cookie);
        return $this->db->get();
    }

    function update($update_data, $id){
        $this->db->update($this->_table3, $update_data, array('id_admin' => $id));
    }
  }     