<?php

class M_artikel_frontpage extends CI_Model{
    private $_table = "tb_artikel";

    function more_artikel(){
        $this->db->select("tb_artikel.gambar, tb_artikel.nama_artikel, tb_artikel.slug, tb_kategori.kategori, tb_artikel.created_at, tb_artikel.deskripsi ");
        $this->db->from("tb_artikel");
        $this->db->join("tb_kategori","tb_kategori.id_kategori = tb_artikel.id_kategori");
        $this->db->where("tb_artikel.is_delete = 0");
        $this->db->limit(6);
        $this->db->order_by("tb_artikel.id_artikel","DESC");
        return $this->db->get()->result();
    }

    function get_artikel($slug){
    	$this->db->where("slug",$slug);
        return $this->db->get("tb_artikel")->result();
    }


    
}