<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $this->load->view('template/head')?>
  <!-- Buat Menampilkan Isi Head -->
    
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    
    <?php $this->load->view('template/header',["page" => "home"])?>
  <!-- Buat Menampilkan Isi Header -->

    <div class="intro-section" id="home-section">
      
      <div class="slide-1" style="background-image: url('<?=base_url();?>assets/frontpage/images/hero_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12">
              <div class="row align-items-center">
                <div class="col-lg-6 mb-4">
                  <h1  data-aos="fade-up" data-aos-delay="100">"Sebaik-baiknya orang diantara kamu adalah orang yang belajar Al-Quran dan mengajarkannya"</h1>
                  <p class="mb-4"  data-aos="fade-up" data-aos-delay="200">Pondok Pesantren Al-Islam didirikan oleh KH. Imam Asfali sejak tahun 1952. Bertempat di Kota Malang.</p>
                  <p data-aos="fade-up" data-aos-delay="300"><a href="<?=base_url('pendaftaran');?>" class="btn btn-primary py-3 px-5 btn-pill">Daftar Sekarang</a></p>

                </div>

                
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>

    
    <div class="site-section courses-title bg-dark" id="courses-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="">
            <h2 class="section-title">Artikel</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section courses-entry-wrap"  data-aos="fade-up" data-aos-delay="100">
      <div class="container">
        <div class="row">

          <div class="owl-carousel col-12 nonloop-block-14">

            <?php
              foreach ($artikel as $e) {
            ?>
            <div class="course bg-white h-100 align-self-stretch">
              <figure class="m-0" style="max-height: 290px; overflow: hidden;">
                <a href="<?=base_url("artikel/baca/".$e->slug);?>"  style="height: 250px; overflow: hidden;">
                  <img src="<?=base_url($e->gambar);?>" alt="Image" class="img-fluid" style="scale: 3;">
                </a>
              </figure>
              <div class="course-inner-text py-4 px-4" style="background-color: white;">
                <span class="course-price"><?=$e->kategori;?></span>
                <h3><a href="<?=base_url("artikel/baca/".$e->slug);?>"><?=$e->nama_artikel;?></a></h3>
                <p>...<?php echo substr($e->deskripsi, 30, 110);?>... </p>
              </div>
              <div class="d-flex border-top stats">
                <div class="py-3 px-4"><span class="icon-users"></span>
                  <?php echo date('H', strtotime($e->created_at)); ?>:<?php echo date('i', strtotime($e->created_at)); ?>, 
                  <?php echo date('d', strtotime($e->created_at)); ?> 
                    <?php 
                      $Bulan = array("January", "February", "Maret", "April", "May", "June", "July", "August", "September", "October", "November", "Desember",);
                      echo $Bulan[date('m', strtotime($e->created_at)) - 1]; 
                    ?> 
                    <?php echo date('Y', strtotime($e->created_at)); ?> </div>
                <div class="py-3 px-4 w-25 ml-auto border-left"></div>
              </div>
            </div>
            <?php
              }
            ?>



            

          </div>

         

        </div>
        <div class="row justify-content-center">
          <div class="col-7 text-center">
            <button class="customPrevBtn btn btn-primary m-1">Prev</button>
            <button class="customNextBtn btn btn-primary m-1">Next</button>
          </div>
        </div>
      </div>
    </div>


    <div class="site-section" id="programs-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-lg-7 text-center"  data-aos="fade-up" data-aos-delay="">
            <h2 class="section-title">Mengapa PP. Al-Islam?</h2>
            <p>Mencetak santri dan hafidz Al-Quran yang beriman, bertaqwa, cerdas, mandiri, kreatif, berakhlaq mulia dan bermanhaj ahlus sunnah wal jama'ah. </p>
          </div>
        </div>
        <div class="row mb-5 align-items-center">
          <div class="col-lg-7 mb-5" data-aos="fade-up" data-aos-delay="100">
            <img src="<?=base_url();?>assets/images/Pondok Image/IMG-20191231-WA0010.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-4 ml-auto" data-aos="fade-up" data-aos-delay="200">
            <h2 class="text-black mb-4">Pendidikan yang Kontemporer</h2>
            <p class="mb-4">Mendidik santri menjadi generasi yang berahlaq mulia dan memiliki pengetahuan yang luas sehingga menjadi insanul kamil </p>

            

          </div>
        </div>

        <div class="row mb-5 align-items-center">
          <div class="col-lg-7 mb-5 order-1 order-lg-2" data-aos="fade-up" data-aos-delay="100">
            <img src="<?=base_url();?>assets/images/Pondok Image/IMG-20191231-WA0014.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-4 mr-auto order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
            <h2 class="text-black mb-4">Tahfidzul Quran</h2>
            <p class="mb-4">Melaksanakan sistem hafalan Al-Quran yang komprehensif dan kondusif serta menjadikan Al-Quran sebagai pedoman sehari-hari.</p>

            

          </div>
        </div>

        <div class="row mb-5 align-items-center">
          <div class="col-lg-7 mb-5" data-aos="fade-up" data-aos-delay="100">
            <img src="<?=base_url();?>assets/images/Pondok Image/IMG-20191231-WA0011.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-4 ml-auto" data-aos="fade-up" data-aos-delay="200">
            <h2 class="text-black mb-4">Generasi Islam</h2>
            <p class="mb-4">Mencetak kader penerus perjuangan yang berkesinambungan dan penggerak dakwah islam</p>

            

          </div>
        </div>

      </div>
    </div>

    <div class="site-section" id="teachers-section">
      <div class="container">

        <div class="row mb-5 justify-content-center">
          <div class="col-lg-7 mb-5 text-center"  data-aos="fade-up" data-aos-delay="">
            <h2 class="section-title">Ustadz Kami</h2>
            <p class="mb-5">Ustadz kami mengedepankan visi dan misi PP AL-ISLAM dengan mendidik santri agar bertakwa kepada Allah SWT</p>
          </div>
        </div>

        <div class="row">

          <?php
              foreach ($ustadz as $e) {
          ?>
          <div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="100">
            <div class="teacher text-center">
              <img src="<?=base_url($e->foto_ustadz);?>" alt="Image" class="img-fluid w-50 rounded-circle mx-auto mb-4">
              <div class="py-2">
                <h3 class="text-black"><?=$e->nama_ustadz;?></h3>
                <p><?=$e->quotes;?></p>
              </div>
            </div>
          </div>
          <?php
              }
          ?>
        </div>
      </div>
    </div>

    <div class="site-section bg-image overlay" style="background-image: url('<?=base_url();?>assets/frontpage/images/hero_1.jpg');">
      <div class="container">
        <div class="row justify-content-center align-items-center">
          <div class="col-md-8 text-center testimony">
            <img src="<?=base_url();?>assets/frontpage/images/photopendiri.jpg" alt="Image" class="img-fluid w-25 mb-4 rounded-circle">
            <h3 class="mb-4">KH. Imam Asfali</h3>
            <blockquote>
              <p>Beliau adalah pendiri pondok pesantren Al-Islam Malang, beliau Wafat pada 30 jumadil akhir 1420 / 3 oktober 1999</p>
            </blockquote>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section pb-0">

      <div class="future-blobs">
        <div class="blob_2">
          <img src="images/blob_2.svg" alt="Image">
        </div>
        <div class="blob_1">
          <img src="images/blob_1.svg" alt="Image">
        </div>
      </div>
      <div class="container">
        <div class="row mb-5 justify-content-center" data-aos="fade-up" data-aos-delay="">
          <div class="col-lg-7 text-center">
            <h2 class="section-title">Kami Menyambut Anda</h2>
          </div>
        </div>
        <div class="row" >
          <div class="col-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.095078955831!2d112.62892911477944!3d-7.989117394246296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd6281aa605d1af%3A0xd2ddcc81259f912a!2sPP.%20Al-Islam!5e0!3m2!1sid!2sid!4v1578827001737!5m2!1sid!2sid" rameborder="0" class="maps-box" allowfullscreen=""></iframe>
          </div>
        </div>
      </div>
    </div>

    <style type="text/css" media="screen">
      .maps-box{
        border:5px black; width: 100%; height: 512px;
      }
        
        @media only screen and (min-width: 1080px) and (max-width: 2340px) and (-webkit-min-device-pixel-ratio: 1){
          .maps-box{
            border:0; height: 352px;
          }
        }  
        @media only screen and (min-width: 768px) and (max-width: 1024px) and (-webkit-min-device-pixel-ratio: 1){
          .maps-box{
            border:0; height: 352px;
          }
        }
        @media only screen and  (max-width: 767px) and (-webkit-min-device-pixel-ratio: 1) and (orientation: portrait){
          .maps-box{
            border:0; height: 352px;
          }
        }  
    </style>

    
     
    
    <?php $this->load->view('template/footer')?>
  <!-- Buat Menampilkan Isi Footer -->

  
    
  </div> <!-- .site-wrap -->

  <?php $this->load->view('template/script1')?>

  <script>
      

      function initMap() {
  var myLatLng = {lat: -7.989091, lng: 112.631138};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Pondok Pesantren Al-Islam Malang'
  });
}
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbrDWU_DUNnoVauWzqkAhTUgdy19nGtsY&callback=initMap"
    async defer></script>
  <!-- Buat Menampilkan Isi Script -->
  
    
  </body>
</html>