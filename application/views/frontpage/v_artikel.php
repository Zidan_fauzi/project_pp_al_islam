<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $this->load->view('template/head')?>
    
    <style>
      .icon-menu{
        display : none;
      }
    </style>
  <!-- Buat Menampilkan Isi Head -->
    
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target" style="display : none">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    
    <?php $this->load->view('template/header',["page" => "artikel"])?>
  <!-- Buat Menampilkan Isi Header -->
    <?php
      foreach ($isi_artikel as $a) { 
    ?>
    <div class="intro-section single-cover" id="home-section">
      
      <div class="slide-1 " style="background-image: url('<?=base_url($a->gambar);?>');" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12">
              <div class="row justify-content-center align-items-center text-center">
                <div class="col-lg-6" style="padding-top: 60px;">
                  <h1 data-aos="fade-up" data-aos-delay="0"><?=$a->nama_artikel;?></h1>
                  <p data-aos="fade-up" data-aos-delay="100">
                    <?php echo date('d', strtotime($a->created_at)); ?> 
                    <?php 
                      $Bulan = array("January", "February", "Maret", "April", "May", "June", "July", "August", "September", "October", "November", "Desember",);
                      echo $Bulan[date('m', strtotime($a->created_at)) - 1]; 
                    ?> 
                    <?php echo date('Y', strtotime($a->created_at)); ?> 
                  </p>
                </div>

                
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    
    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 mb-5 m-auto">
            <div class="mb-5">
              <?=$a->deskripsi;?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
      };
    ?>

    <div class="site-section courses-title bg-dark" id="courses-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="">
            <h2 class="section-title">Related Article</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section courses-entry-wrap"  data-aos="fade" data-aos-delay="100">
      <div class="container">
        <div class="row">

          <div class="owl-carousel col-12 nonloop-block-14">

            <?php
              foreach ($more_artikel as $e) {
            ?>
            <div class="course bg-white h-100 align-self-stretch">
              <figure class="m-0" style="max-height: 290px; overflow: hidden;">
                <a href="<?=base_url("artikel/baca/".$e->slug);?>"><img src="<?=base_url($e->gambar);?>" alt="Image" class="img-fluid" style="scale: 3;"></a>
              </figure>
              <div class="course-inner-text py-4 px-4">
                <span class="course-price"><?=$e->kategori;?></span>
                <h3><a href="<?=base_url("artikel/baca/".$e->slug);?>"><?=$e->nama_artikel;?></a></h3>
                <p>...<?php echo substr($e->deskripsi, 30, 110);?>... </p>
              </div>
              <div class="d-flex border-top stats">
                <div class="py-3 px-4"><span class="icon-users"></span> 
                  <?php echo date('H', strtotime($e->created_at)); ?>:<?php echo date('i', strtotime($e->created_at)); ?>, 
                  <?php echo date('d', strtotime($e->created_at)); ?> 
                    <?php 
                      $Bulan = array("January", "February", "Maret", "April", "May", "June", "July", "August", "September", "October", "November", "Desember",);
                      echo $Bulan[date('m', strtotime($e->created_at)) - 1]; 
                    ?> 
                    <?php echo date('Y', strtotime($e->created_at)); ?> 
                </div>
                <div class="py-3 px-4 w-25 ml-auto border-left"></div>
              </div>
            </div>
            <?php
              };
            ?>

          </div>

         

        </div>
        <div class="row justify-content-center">
          <div class="col-7 text-center">
            <button class="customPrevBtn btn btn-primary m-1">Prev</button>
            <button class="customNextBtn btn btn-primary m-1">Next</button>
          </div>
        </div>
      </div>
    </div>
     
    <?php $this->load->view('template/footer')?>
  <!-- Buat Menampilkan Isi Footer -->

  
    
  </div> <!-- .site-wrap -->

  <?php $this->load->view('template/script1')?>
  <!-- Buat Menampilkan Isi Script -->
    
  </body>
</html>