<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>
                    Manage Artikel
                    <div class="page-title-subheading">Manage Artikel Kesukaanmu</div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    <a href="<?php echo base_url('Manage_artikel/indexCreate'); ?>" class="btn-shadow btn btn-info">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Create Your Artikel
                    </a>
                </div>
            </div>    
        </div>
    </div>
    <?php echo $this->session->flashdata('create'); ?>
    <?php echo $this->session->flashdata('edit'); ?>
    <?php echo $this->session->flashdata('delete'); ?>
    <div class="card mb-3">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <i class="header-icon pe-7s-albums mr-3 text-muted opacity-6"></i> 
                Manage Artikel
            </div>
            <div class="btn-actions-pane-right actions-icon-btn"></div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Artikel</th>
                        <th>Slug</th>
                        <th>Dibuat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($artikel as $dataArtikel) { ?>
                    <tr>
                        <td><?php echo $no++?></td>
                        <td><?php echo $dataArtikel['nama_artikel'];?></td>
                        <td><?php echo $dataArtikel['slug'];?></td>
                        <td><time class="timeago" datetime="<?php echo $dataArtikel['updated_at']; ?>" title="<?php echo date("j F Y H:i:s", strtotime($dataArtikel['updated_at'])); ?>"></td>
                        <td><?php echo $dataArtikel['kategori'];?></td>
                        <td>
                            <div class="text-center">
                                <div class="dropdown">
                                    <button type="button" aria-haspopup="true" data-toggle="dropdown" class="btn btn-primary">   
                                        Info 
                                    </button>
                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-right dropdown-menu rm-pointers">
                                        <div class="dropdown-menu-header">
                                            <div class="dropdown-menu-header-inner bg-primary">
                                                <div class="menu-header-image" style="background-image: url('assets/images/dropdown-header/abstract3.jpg');"></div>
                                                <div class="menu-header-content"><h5 class="menu-header-title">Settings</h5><h6 class="menu-header-subtitle">Manage all of your options</h6></div>
                                            </div>
                                        </div>
                                        <div class="grid-menu grid-menu-xl grid-menu-3col">
                                            <div class="no-gutters row">
                                                <div class="col-sm-6 col-xl-6">
                                                    <button class="delete-button btn-icon-vertical btn-square btn-transition btn btn-outline-link"data-toggle="modal" data-target="#exampleModal" data-base_url="<?php echo base_url('Manage_artikel'); ?>" data-id='<?php echo $dataArtikel['id_artikel']; ?>' data-judul='<?php echo $dataArtikel['nama_artikel']; ?>'>
                                                        <i class="pe-7s-trash btn-icon-wrapper btn-icon-lg mb-3"></i>
                                                        Delete Artikel
                                                    </button>
                                                    <!-- href="<?php echo base_url('Manage_artikel/detail/').$dataArtikel['id_artikel'];?>" -->
                                                </div>
                                                <div class="col-sm-6 col-xl-6">
                                                   <a href="<?php echo base_url('Manage_artikel/indexUpdate/').$dataArtikel['id_artikel'];?>" class="btn-icon-vertical btn-square btn-transition btn btn-outline-link"><i class="pe-7s-note btn-icon-wrapper btn-icon-lg mb-3"> </i>Edit Artikel</a>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="nav flex-column">
                                            <li class="nav-item-divider nav-item"></li>
                                            <li class="nav-item-btn text-right nav-item">
                                                <a href="<?php echo base_url('Manage_artikel/detail/').$dataArtikel['id_artikel'];?>" class="btn-pill btn btn-success btn-sm">Detail</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>    
</div>