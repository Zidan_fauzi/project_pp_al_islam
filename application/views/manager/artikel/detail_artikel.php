<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-bookmarks icon-gradient bg-strong-bliss">
                    </i>
                </div>
                <div>
                    Detail Artikel
                    <div class="page-title-subheading">Baca Artikel Kesukaanmu Disini.
                    </div>
                </div>
            </div>
        </div>
    </div>            
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-header">
                    <i class="header-icon pe-7s-bookmarks icon-gradient bg-plum-plate"> </i>
                    <?php echo $artikel->nama_artikel;?>
                </div>
                <div class="card-header">
                    <div>
                        <h6>Artikel ini ditulis oleh : <?php echo $artikel->nama;?> <strong>|</strong> <time class="timeago" datetime="<?php echo $artikel->updated_at; ?>" title="<?php echo date("j F Y H:i:s", strtotime($artikel->updated_at)); ?>"></time>
                            <strong>|</strong> <?php echo $artikel->kategori;?>
                        </h6>
                    </div>
                </div>
                <div class="card-body">
                    <div class="scroll-area-md">
                        <div class="scrollbar-container ps--active-y ps">
                            <div class="text-center d-block">
                                <div class="col-md-12 mb-2 p-0">
                                    <img src='<?php echo base_url($artikel->gambar); ?>' style="max-width: 35%;">
                                </div>
                            </div>
                            <div class="divider"></div>
                            <?php echo $artikel->deskripsi;?>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;">
                                    
                                </div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; height: 200px; right: 0px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 50px;">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-block text-right card-footer">
                    <a href="<?php echo base_url().'Manage_artikel'?>" class="btn btn-warning btn-lg">Back to Manage Artikel</a>
                </div>
            </div>
        </div>
    </div>
</div>