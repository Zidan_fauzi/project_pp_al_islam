<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Create Artikel
                    <div class="page-title-subheading">Buat Artikel Kesukaanmu Disini.
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Create Artikel</h5>
                    <form action="<?php echo (base_url('Manage_artikel/create')); ?>" method="post" enctype="multipart/form-data">
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class="">Judul</label>
                            <input name="namaArtikel" placeholder="" type="text" class="form-control" id="judul" onkeyup="createslug()" required>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class="">Slug</label>
                            <input name="slug" placeholder="" type="text" class="form-control" id="slug" readonly>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleSelect" class="">Kategori</label>
                            <select name="kategori"  id="kategori" class="form-control" required>
                                <option value=""> - Pilih Kategori -</option>
                                <?php
                                    foreach ($kategori as $datakategori) {
                                        echo "<option value='$datakategori->id_kategori'>".$datakategori->kategori."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleText" class="">Deskripsi</label>
                            <textarea name="deskripsi" class="form-control" required></textarea>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleFile" class="">Gambar</label>
                            <input type="file" accept="gambar/*" name="gambar" class="form-control-file dropify" required>
                            <small class="form-text text-danger">* Jika Gambar yang kamu upload tidak muncul gambar terlalu besar ukurannya.</small>
                            <small class="form-text text-danger">* Gambar harus kurang dari 2MB.</small>
                        </div>
                        <button class="mt-1 btn btn-primary" onsubmit="return validasi()">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>