<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Update Artikel
                    <div class="page-title-subheading">Perbarui Artikel Kesukaanmu Disini !
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Update Artikel</h5>
                    <form action="<?php echo base_url('Manage_artikel/edit/'.$artikel->id_artikel); ?>" method="post" enctype="multipart/form-data">
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class="">Judul</label>
                            <input name="namaArtikel" placeholder="" type="text" class="form-control" id="judul" value="<?php echo ($artikel->nama_artikel) ?>" onkeyup="createslug()" required>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class="">Slug</label>
                            <input name="slug" placeholder="" type="text" class="form-control" id="slug" value="<?php echo ($artikel->slug) ?>" readonly>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleSelect" class="">Kategori</label>
                            <select name="kategori"  id="kategori" class="form-control" required>
                                <option value=""> - Pilih Kategori -</option>
                                <?php
                                      foreach ($kategori as $datakategori) {
                                        ?>
                                            <option value="<?php echo ($datakategori->id_kategori) ?>" 
                                            <?php if ($datakategori->id_kategori == $artikel->id_kategori){echo 'selected';}?>>
                                            <?php echo $datakategori->kategori ?>
                                            </option>";
                                        <?php
                                      }
                                  ?>
                            </select>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleText" class="">Deskripsi</label>
                            <textarea name="deskripsi" class="form-control" id="deskripsi" required><?php echo $artikel->deskripsi;?></textarea>
                        </div>
                        <div class="position-relative form-group" id="masukkan">
                            <label for="exampleFile" class="">Gambar</label>
                            <input type="file" accept="gambar/*" name="gambar" class="form-control-file dropify">
                            <small class="form-text text-muted">Jika Gambar yang kamu upload tidak muncul gambar terlalu besar ukurannya.</small>
                            <small class="form-text text-muted">Gambar harus kurang dari 2MB.</small>
                            <input type="button" class="btn btn-danger cancel" value="Cancel"/>
                        </div>
                        <div class="position-relative form-group" id="tampilan">
                            <label for="exampleFile" class="">Image</label>
                            <div class="text-center d-block">
                                <div class="col-md-12 mb-2 p-0">
                                    <img src='<?php echo base_url($artikel->gambar); ?>' style="max-width: 35%;">
                                </div>
                            </div>
                            <input type="button" class="btn btn-success change" value="Change"/>
                        </div>
                        <div class="modal-footer clearfix">
                            <div class="float-right">
                                <button type="button" class="btn btn-primary btn-lg" onclick="checkData()">Submit</button>
                                <button type="button" onclick="this.form.submit()" class="btn_send" style="display: none;"></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function checkData(){
        if ($('#judul').val() != "" && $('#slug').val() != "" && $('#kategori').val() != "" && $('#deskripsi').val() != "") {
            $('.btn_send').trigger('click');
        } else {
            alert("Please Fill Out All Form !");
            return;
        }
    }
</script>
<script>
    $('#masukkan').hide();             
    $('.cancel').hide();   
              
    $('.cancel').click(function(){
        $('#masukkan').hide();
        $('#tampilan').show();
        $('.change').show();
        $('.cancel').hide(); 
    });

    $('.change').click(function(){
        $('#masukkan').show();
        $('#tampilan').hide();     
        $('.cancel').show(); 
        $('.change').hide();  
    });
</script>