<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Update Santri
                    <div class="page-title-subheading">Perbarui Santri Yang Sudah Daftar !
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Update Santri</h5>
                    <form action="<?php echo base_url('Manage_santri/edit/').$santri->id_santri; ?>" method="post" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="">Nama Lengkap <strong class="text-danger">*</strong></label>
                                <div class="position-relative form-group">
                                    <input name="nama" id="nama" type="text" class="form-control" value="<?php echo $santri->nama; ?>" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="">Jenis Kelamin <strong class="text-danger">*</strong></label>
                                <div class="position-relative form-group">
                                    <select id="jenisK" name="JenisKelamin" class="form-control"required>
                                        <option value="">-Pilih-</option>
                                        <option value="1" <?php if ($santri->jenis_kelamin == 1){ echo "selected";}?>>Laki-Laki</option>
                                        <option value="2"<?php if ($santri->jenis_kelamin == 2){ echo "selected";}?>>Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="">Tempat Lahir <strong class="text-danger">*</strong></label>
                                <div class="position-relative form-group">
                                    <input name="tempat_lahir" id="tempat" type="text" class="form-control" value="<?php echo $santri->tempat_lahir; ?>" required>
                                </div>                                  
                            </div>
                            <div class="col-md-12">
                                <label for="">Tanggal Lahir <strong class="text-danger">*</strong></label>
                                <div class="position-relative form-group">
                                    <input type="date" class="form-control" id="tanggal" value="<?php echo $santri->tanggal_lahir; ?>" name="tanggal_lahir" required>
                                </div>  
                            </div>
                            <div class="col-md-12">
                                <label for="">Nama Orangtua <strong class="text-danger">*</strong></label>
                                <div class="position-relative form-group">
                                    <input name="nama_ortu" type="text" id="namaOrtu" class="form-control" value="<?php echo $santri->nama_orangtua; ?>" required>
                                </div>                                  
                            </div>
                            <div class="col-md-12">
                                <label for="">Nomor Handphone <strong class="text-danger">*</strong></label>
                                <div class="position-relative form-group">
                                    <input name="no_HP" type="tel" id="noTelp" value="<?php echo $santri->no_handphone; ?>" class="form-control" required>
                                </div>                                  
                            </div>
                            <div class="col-md-12">
                                <label for="">Alamat <strong class="text-danger">*</strong></label>
                                <div class="position-relative form-group">
                                    <textarea name="alamat" class="form-control" id="alamat" value="0" required><?php echo $santri->alamat; ?></textarea>
                                </div>                                        
                            </div>
                            <div class="col-md-12">
                                <div class="position-relative form-group" id="masukkan">
                                    <label for="exampleFile" class="">Image</label>
                                    <div class="position-relative form-group">
                                        <input type="file" accept="gambar_santri/*" name="gambar_santri" class="form-control-file dropify" required data-height="200">
                                    </div>
                                    <label class="form-text text-danger">* Jika Gambar yang kamu upload tidak muncul, gambar terlalu besar ukurannya.</label>
                                    <label class="form-text text-danger">* Gambar harus kurang dari 2MB dan berukuran 4x6.</label>
                                    <input type="button" class="btn btn-danger cancel" value="Cancel"/>
                                </div>
                                <div class="position-relative form-group" id="tampilan">
                                    <label for="exampleFile" class="">Image</label>
                                    <div class="text-center d-block">
                                        <div class="col-md-12 mb-2 mr-5 p-0">
                                            <img src='<?php echo base_url($santri->gambar_profile); ?>' style="max-width: 35%;">
                                        </div>
                                    </div>
                                    <input type="button" class="btn btn-success change" value="Change"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer clearfix">
                            <div class="float-right">
                                <button type="button" class="btn btn-primary btn-lg" onclick="checkData()">Submit</button>
                                <button type="button" onclick="this.form.submit()" class="btn_send" style="display: none;"></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkData(){
        if ($('#nama').val() != "" && $('#jenisK').val() != "" && $('#tempat').val() != "" && $('#tanggal').val() != "" && $('#namaOrtu').val() != "" && $('#noTelp').val() != "" && $('#alamat').val() != "") {
            $('.btn_send').trigger('click');
        } else {
            alert("Please Fill Out All Form !");
            return;
        }
    }
</script>
<script>
    $('#masukkan').hide();             
    $('.cancel').hide();   
              
    $('.cancel').click(function(){
        $('#masukkan').hide();
        $('#tampilan').show();
        $('.change').show();
        $('.cancel').hide(); 
    });

    $('.change').click(function(){
        $('#masukkan').show();
        $('#tampilan').hide();     
        $('.cancel').show(); 
        $('.change').hide();  
    });
</script>