<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>
                    Manage Santri Baru
                    <div class="page-title-subheading">Manage Santri Yang Belum Terdaftar di Pondokanmu.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <i class="header-icon pe-7s-add-user mr-3 text-muted opacity-6"></i> 
                Manage Pendaftaran Santri
            </div>
            <div class="btn-actions-pane-right actions-icon-btn"></div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Santri</th>
                        <th>Alamat</th>
                        <th>Dibuat</th>
                        <th>Tempat,<br> Tanggal Lahir</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($santri as $datasantri) { ?>
                    <tr>
                        <td><?php echo $no++?></td>
                        <td><?php echo $datasantri['nama'];?></td>
                        <td><?php echo $datasantri['alamat'];?></td>
                        <td>
                            <time class="timeago" datetime="<?php echo $datasantri['created_at']; ?>" title="<?php echo date("j F Y H:i:s", strtotime($datasantri['created_at'])); ?>"></time>
                        </td>                                
                        <td><?php echo $datasantri['tempat_lahir'];?>, <?php echo $datasantri['tanggal_lahir'];?></td>
                        <td>
                            <?php if ($datasantri['status'] == 0) { ?>
                                <div class="mb-2 mr-2 badge badge-warning" style="color: white;">Pending</div>
                            <?php } ?>
                            <?php if ($datasantri['status'] == 1) { ?>
                                <div class="mb-2 mr-2 badge badge-success">Diterima</div>
                            <?php } ?>
                            <?php if ($datasantri['status'] == 2) { ?>
                                <div class="mb-2 mr-2 badge badge-danger">Ditolak</div>
                            <?php } ?>
                        </td>
                        <td>
                            <div class="text-center">
                                <div class="dropdown">
                                    <button type="button" aria-haspopup="true" data-toggle="dropdown" class="btn btn-primary">   
                                        Info 
                                    </button>
                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-right dropdown-menu rm-pointers">
                                        <div class="dropdown-menu-header">
                                            <div class="dropdown-menu-header-inner bg-primary">
                                                <div class="menu-header-image" style="background-image: url('assets/images/dropdown-header/abstract3.jpg');"></div>
                                                <div class="menu-header-content"><h5 class="menu-header-title">Settings</h5><h6 class="menu-header-subtitle">Manage all of your options</h6></div>
                                            </div>
                                        </div>
                                        <div class="grid-menu grid-menu-xl grid-menu-3col">
                                            <div class="no-gutters row">
                                                <?php if ($datasantri['status'] == 0) { ?>
                                                    <div class="col-sm-6 col-xl-6">
                                                        <button class="delete-button btn-icon-vertical btn-square btn-transition btn btn-outline-link"data-toggle="modal" data-target="#exampleModalSantriAccept" data-base_url="<?php echo base_url('Manage_requestUser'); ?>" data-idSantri='<?php echo $datasantri['id_santri']; ?>' data-namaSantri='<?php echo $datasantri['nama']; ?>'>
                                                            <i class="pe-7s-check btn-icon-wrapper btn-icon-lg mb-3"></i>
                                                            Terima Santri Masuk
                                                        </button>
                                                    </div>
                                                    <div class="col-sm-6 col-xl-6">
                                                        <button class="delete-button btn-icon-vertical btn-square btn-transition btn btn-outline-link"data-toggle="modal" data-target="#exampleModalSantriReject" data-base_url="<?php echo base_url('Manage_requestUser'); ?>" data-idSantri='<?php echo $datasantri['id_santri']; ?>' data-namaSantri='<?php echo $datasantri['nama']; ?>'>
                                                            <i class="pe-7s-close-circle btn-icon-wrapper btn-icon-lg mb-3"></i>
                                                            Tolak Santri Masuk
                                                        </button>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($datasantri['status'] == 1) { ?>
                                                    <div class="col-sm-12 col-xl-12">
                                                        <button class="delete-button btn-icon-vertical btn-square btn-transition btn btn-outline-link"data-toggle="modal" data-target="#exampleModalSantriReject" data-base_url="<?php echo base_url('Manage_requestUser'); ?>" data-idSantri='<?php echo $datasantri['id_santri']; ?>' data-namaSantri='<?php echo $datasantri['nama']; ?>'>
                                                            <i class="pe-7s-close-circle btn-icon-wrapper btn-icon-lg mb-3"></i>
                                                            Tolak Santri Masuk
                                                        </button>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($datasantri['status'] == 2) { ?>
                                                    <div class="col-sm-12 col-xl-12">
                                                        <button class="delete-button btn-icon-vertical btn-square btn-transition btn btn-outline-link"data-toggle="modal" data-target="#exampleModalSantriAccept" data-base_url="<?php echo base_url('Manage_requestUser'); ?>" data-idSantri='<?php echo $datasantri['id_santri']; ?>' data-namaSantri='<?php echo $datasantri['nama']; ?>'>
                                                            <i class="pe-7s-check btn-icon-wrapper btn-icon-lg mb-3"></i>
                                                            Terima Santri Masuk
                                                        </button>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>    
</div>