			<div class="app-wrapper-footer">
			    <div class="app-footer">
			        <div class="app-footer__inner">
			            <div class="app-footer-right">
			                <ul class="nav">
			                    <li class="nav-item">
			                        <div href="javascript:void(0);" class="nav-link">
			                            Copyright ©2020 All rights reserved | This template is made with by Colorlib
			                        </div>
			                    </li>
			                </ul>
			            </div>
			        </div>
			    </div>
			</div>  
		</div>
	</div>
</div>
<div class="modal fade" id="ModalCreateKategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('manage_kategori/create')?>" method="post">
                    <div class="position-relative form-group">
                        <label for="exampleEmail" class="">Kategori</label>
                        <input name="kategori" placeholder="" type="text" class="form-control" id="judul" onkeyup="createslug()" required>
                    </div>
                    <div class="position-relative form-group">
                        <label for="exampleEmail" class="">slug</label>
                        <input name="slug" placeholder="" type="text" class="form-control" id="slug" readonly>
                    </div>
                    <div class="position-relative row form-check">
                        <div class="col-sm-10 offset-sm-2">
                            <button class="btn btn-success">Submit</button>
                </form>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
          </div>
      </div>
            </div>
        </div>
    </div>   
</div>

<script type="text/javascript" src="<?php echo base_url('assets/scripts/jquery.timeago.js')?>"></script>
<script>
  jQuery(document).ready(function() {
    jQuery("time.timeago").timeago();
  });
</script>
<script type="text/javascript">
    HideTag();
    var fileVal = document.getElementById("inputanFoto");
    function HideTag() {
        $('#email-ngecheck2').hide();
        $('#email-ngecheck').hide();
        $('#nama-ngecheck2').hide();
        $('#gambar-ngecheck2').hide();
    }
    function myFunctionHeader(){
        HideTag();
        var fileVal = document.getElementById("inputanFotoHeader");
        if(fileVal.value != ""){
            $('.btn_sendHeader').trigger('click');
        }else if ($('#namaHeader').val() != $('#namaHeadercheck').val()) {
            $.ajax({
                url:"<?php echo base_url('Dashboard/profil_editByname/'.$this->session->userdata('id_admin')) ?>",
                method:"POST",
                data:{nama: $('#namaHeader').val(), password: $('#passwordHeader').val()},
                success:function(data)
                {
                    alert("Please Refresh Page to Take Effect !");
                }
            })
        }else if($('#emailHeader').val() != $('#emailHeadercheck').val()){
            $.ajax({
                url:"<?php echo base_url(); ?>Manage_admin/checkDataByEmail",
                method:"POST",
                data:{email: $('#emailHeader').val()},
                success:function(data)
                {
                    console.log(data);
                    if(data != "")
                    {
                        $('#email-ngecheck').show();
                    }
                    else
                    {
                        $('.btn_sendHeader').trigger('click');
                    }
                }
            })
        }else if ($('#passwordHeader').val() != "" || $('#passwordHeader').val() != " ") {
            $.ajax({
                url:"<?php echo base_url('Dashboard/profil_editByname/'.$this->session->userdata('id_admin')) ?>",
                method:"POST",
                data:{nama: $('#namaHeader').val(), password: $('#passwordHeader').val()},
                success:function(data)
                {
                    alert("Please Refresh Page to Take Effect !");
                }
            })
        }
    }
</script>
<script type="text/javascript" src="<?php echo base_url().'assets/dropify/dropify.min.js'?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.dropify').dropify({
			messages: {
                default: 'Drag atau drop untuk memilih gambar',
                replace: 'Ganti',
                remove:  'Hapus',
                error:   'error'
            }
		});
	});
</script>
<script src="<?php echo (base_url('assets/scripts/ckeditor/ckeditor.js'))?>"></script>
<script type="text/javascript">
$(function () {
    CKEDITOR.replace('deskripsi');
  })
</script>
<script type="text/javascript">
  function createslug(){
      var judul = $('#judul').val();
      $('#slug').val(slugify(judul));
  }
  function slugify(text){
      return text.toString().toLowerCase()
              .replace(/\s+/g, '-')           // Replace spaces with 
              .replace(/[\W_]+/g, "-")       // Remove all non-word chars
              .replace(/\-\-+/g, '-')         // Replace multiple - with single -
              .replace(/^-+/, '')             // Trim - from start of text
              .replace(/-+$/, '');            // Trim - from end of text

  }
</script>

<script type="text/javascript">
$('.delete-button').click(function(){
    var id=$(this).attr('data-id');
    var idustadz=$(this).attr('data-idustadz');
    var idkategori=$(this).attr('data-idkategori');
    var idSantri=$(this).attr('data-idSantri');
    var idadmin=$(this).attr('data-idadmin');
    // ============================================================================================
    var base_url = $(this).attr('data-base_url');
    // ============================================================================================
    var judul = $(this).attr('data-judul');
    var admin = $(this).attr('data-admin');
    var ustadz = $(this).attr('data-ustadz');
    var judulkategori = $(this).attr('data-judulkategori');
    var judulSantri = $(this).attr('data-judulSantri');
    var namaSantri = $(this).attr('data-namaSantri');
    // ============================================================================================
    $('#modalDelete').attr('href',base_url+'/delete/'+id);
    $('#modalDeleteUstadz').attr('href',base_url+'/delete/'+idustadz);
    $('#modalDeleteadmin').attr('href',base_url+'/delete/'+idadmin);
    $('#modalDeletekategori').attr('href',base_url+'/delete/'+idkategori);
    $('#modalDeleteSantri').attr('href',base_url+'/delete/'+idSantri);
    $('#modalAcceptSantri').attr('href',base_url+'/accept/'+idSantri);
    $('#modalRejectSantri').attr('href',base_url+'/reject/'+idSantri);
    // ============================================================================================
    document.getElementById("modalDeleteJudul").innerHTML = "Apakah Kamu yakin ingin menghapus Artikel <a href="+base_url+"/detail/"+id+"><strong>"+judul+"</strong></a> ?";
    document.getElementById("modalDeleteJudulUstadz").innerHTML = "Apakah Kamu yakin ingin menghapus Ustadz <a href="+base_url+"/indexUpdate/"+id+"><strong>"+ustadz+"</strong></a> ?";
    document.getElementById("modalDeleteJudulKategori").innerHTML = "Apakah Kamu yakin ingin menghapus Kategori <a href="+base_url+"/indexUpdate/"+idkategori+"><strong>"+judulkategori+"</strong></a> Ini Akan Menghapus Artikel dengan Kategori <a href="+base_url+"/indexUpdate/"+idkategori+"><strong>"+judulkategori+"</strong></a> ?";
    document.getElementById("modalDeleteJudulAdmin").innerHTML = "Apakah Kamu yakin ingin menghapus Admin dengan nama "+admin+" ?";
    document.getElementById("modalDeleteJudulSantri").innerHTML = "Apakah Kamu yakin ingin menghapus Santri Dengan Nama <a href="+base_url+"/indexUpdate/"+idSantri+"><strong>"+judulSantri+"</strong></a> ?";
    document.getElementById("modalNamaSantriAccept").innerHTML = "Apakah Kamu yakin ingin Menerima Santri Dengan Nama <strong>"+namaSantri+"</strong> ?";
    document.getElementById("modalNamaSantriReject").innerHTML = "Apakah Kamu yakin ingin Menolak Santri Dengan Nama <strong>"+namaSantri+"</strong> ?";
})
</script>
</div>
</body>
<!-- Modal -->
</html>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Artikel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="modalDeleteJudul" class="mb-0"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="modalDelete" type="button" class="btn btn-danger" style="color: white;">Delete Article</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModaldeleteUstadz" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Ustadz</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="modalDeleteJudulUstadz" class="mb-0"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="modalDeleteUstadz" type="button" class="btn btn-danger" style="color: white;">Delete Ustadz</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModaldeleteKategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="modalDeleteJudulKategori" class="mb-0"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="modalDeletekategori" type="button" class="btn btn-danger" style="color: white;">Delete Kategori</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModaldeleteAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="modalDeleteJudulAdmin" class="mb-0"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="modalDeleteadmin" type="button" class="btn btn-danger" style="color: white;">Delete Admin</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalSantriDel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Santri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="modalDeleteJudulSantri" class="mb-0"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="modalDeleteSantri" type="button" class="btn btn-danger" style="color: white;">Delete Santri</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalSantriAccept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menerima Santri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="modalNamaSantriAccept" class="mb-0"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="modalAcceptSantri" type="button" class="btn btn-success" style="color: white;">Terima Santri</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalSantriReject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menolak Santri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="modalNamaSantriReject" class="mb-0"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a id="modalRejectSantri" type="button" class="btn btn-danger" style="color: white;">Tolak Santri</a>
            </div>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<div id="uploadimageModal" class="modal" role="dialog" style="background-color: white;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Photo profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="cursor: pointer;">×</span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div id="image_demo" style="margin-top:30px; height:350px;"></div>
                </div>
                <div class="text-center">
                    <button class="btn crop-rotate" data-deg="-90" style="cursor: pointer;"><i class="fas fa-undo"></i></button>
                    <button class="btn crop-rotate" data-deg="90" style="cursor: pointer;"><i class="fas fa-redo"></i></button>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success crop_image"  style="cursor: pointer;">Upload</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" style="cursor: pointer;">Close</button>
            </div>
        </div>
    </div>
</div><br>
<!--===============================================================================================-->