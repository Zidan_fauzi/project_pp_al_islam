<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>
                    Tambahkan Ustadz
                    <div class="page-title-subheading">Tambahkan Ustadz Baru Disini !
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Tambahkan Ustadz</h5>
                    <form action="<?php echo (base_url('Manage_ustadz/create')); ?>" method="post" enctype="multipart/form-data">
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <label for="exampleFile" class="text-center">Nama Ustadz</label>
                                <div class="position-relative form-group">
                                    <input type="text" name="namaUstadz" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <label for="exampleFile" class="text-center">Kata-Kata Mutiara</label>
                                <div class="position-relative form-group">
                                    <input type="text" name="quotesUstadz" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="exampleFile" class="text-center">Image</label><br>
                                    <!-- <input type="file" accept="gambar/*" id="inputFile" name="gambar" class="form-control-file dropfiy" required>
                                    <small class="form-text text-danger">* Jika Gambar yang kamu upload tidak muncul gambar terlalu besar ukurannya.</small>
                                    <small class="form-text text-danger">* Gambar harus kurang dari 2MB.</small> -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <div class="card-title">Full Image</div>
                                                    <div>
                                                        <input type="file" class="dropify" id="img-input" data-height="300" accept="image/*" name="images" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <div class="card-title">Crop Image</div>  
                                                    <div class="uploaded_image"></div>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="mt-1 btn btn-primary text-light">Submit</button>
                        <button style="display: none;" class="btn_open" data-toggle="modal" data-target="#uploadimageModal">Daftar</button>
                        <!-- <button type="button" onclick="this.form.submit()" class="btn_send" style="display: none;"></button> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>  
(function(){ $(document).ready(function(){
    $image_crop = $('#image_demo').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: {
        width:200,
        height:200,
        // type:'square' 
        type: 'circle'
        },
        boundary:{
        width:300,
        height:300
        },
        enableOrientation: true
    });

    $('#img-input').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
        $image_crop.croppie('bind', {
            url: event.target.result,
        }).then(function(){
            console.log('jQuery bind complete');
        });
        }
        reader.readAsDataURL(this.files[0]);
        $('#uploadimageModal').modal('show');
    });

    $('.crop-rotate').on('click', function(event) {
        $image_crop.croppie('rotate',
            parseInt($(this).data('deg'))
        );
    });

    $('.crop_image').click(function(event){
        $image_crop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
        }).then(function(response){
        $.ajax({
            url:"upload_foto/",
            type: "POST",
            data:{"images": response},
            success:function(data)
            {
            $('#uploadimageModal').modal('hide');
            $('.uploaded_image').html(data);
            
            }
        });
        var img = document.getElementById('img-profile');
            img.style.display="none";
        })
    });

}); 
})(jQuery); 
</script>