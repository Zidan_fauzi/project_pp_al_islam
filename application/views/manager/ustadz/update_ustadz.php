<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>
                    Ubah Ustadz
                    <div class="page-title-subheading">Perbarui Data Ustadz Disini!
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Ubah Ustadz</h5>
                    <form action="<?php echo (base_url('Manage_ustadz/update/'.$Data->id_ustadz)); ?>" method="post" enctype="multipart/form-data">
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <label for="exampleFile" class="text-center">Nama Ustadz</label>
                                <div class="position-relative form-group">
                                    <input type="text" id="namaUstadz" name="namaUstadz" value="<?php echo $Data->nama_ustadz; ?>" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <label for="exampleFile" class="text-center">Kata-Kata Mutiara</label>
                                <div class="position-relative form-group">
                                    <input type="text" id="quotesUstadz" name="quotesUstadz"  value="<?php echo $Data->quotes; ?>" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="card-title">Full Image</div>
                                            <div>
                                                <input type="file" class="dropify" id="img-input" data-height="300" accept="image/*" name="images">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="masukkan">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="card-title">Crop Image</div>  
                                            <div class="uploaded_image"></div>  
                                        </div>
                                        <input type="button" class="btn btn-danger cancel" value="Cancel"/>
                                    </div>
                                </div>
                                <div class="col-md-6" id="tampilan">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="card-title">Old Image</div>  
                                            <img src="<?php echo base_url($Data->foto_ustadz); ?>" alt="Your Image">
                                        </div>
                                        <input type="button" class="btn btn-success change" value="Change"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a href="<?php echo base_url('Manage_ustadz'); ?>" class="mt-1 btn btn-warning text-light">Back</a>
                            <a class="mt-1 btn btn-primary text-light" onclick="checkData()">Submit</a>
                        </div>
                        <button type="button" onclick="this.form.submit()" class="btn_send" style="display: none;"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkData(){
        console.log($('#namaUstadz').val());
        if ($('#namaUstadz').val() != "" && $('#quotesUstadz').val() != "") {
            $('.btn_send').trigger('click');
        }else if ($('#namaUstadz').val() == " " && $('#quotesUstadz').val() == " ") {
            alert("Please Fill Out All Form !");
            return;
        } else {
            alert("Please Fill Out All Form !");
            return;
        }
    }   
</script>
<script>
    $('#masukkan').hide();             
    $('.cancel').hide();   
    $('#cancelBtn').hide();   
              
    $('.cancel').click(function(){
        $('#masukkan').hide();
        $('#tampilan').show();
        $('.change').show();
        $('#changeBtn').show();
        $('.cancel').hide(); 
        $('#cancelBtn').hide(); 
    });

    $('.change').click(function(){
        $('#masukkan').show();
        $('#tampilan').hide();     
        $('.cancel').show(); 
        $('#cancelBtn').show(); 
        $('.change').hide();  
        $('#changeBtn').hide();  
    });
</script>

<script>  
(function(){ $(document).ready(function(){
    $image_crop = $('#image_demo').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: {
        width:200,
        height:200,
        // type:'square' 
        type: 'circle'
        },
        boundary:{
        width:300,
        height:300
        },
        enableOrientation: true
    });

    $('#img-input').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
        $image_crop.croppie('bind', {
            url: event.target.result,
        }).then(function(){
            console.log('jQuery bind complete');
        });
        }
        reader.readAsDataURL(this.files[0]);
        $('#uploadimageModal').modal('show');
    });

    $('.crop-rotate').on('click', function(event) {
        $image_crop.croppie('rotate',
            parseInt($(this).data('deg'))
        );
    });

    $('.crop_image').click(function(event){
        $image_crop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
        }).then(function(response){
        $.ajax({
            url:"../upload_foto/",
            type: "POST",
            data:{"images": response},
            success:function(data)
            {
                $('#uploadimageModal').modal('hide');
                $('.uploaded_image').html(data);
                $('.change').trigger('click');
            }
        });
        var img = document.getElementById('img-profile');
            img.style.display="none";
        })
    });

}); 
})(jQuery); 
</script>