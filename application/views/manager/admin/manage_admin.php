<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>
                    Manage Admin
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    <a href="<?php echo base_url('Manage_admin/indexCreate'); ?>" class="btn-shadow btn btn-info">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-business-time fa-w-20"></i>
                        </span>
                        Tambahkan Admin
                    </a>
                </div>
            </div>    
        </div>
    </div>
    <?php echo $this->session->flashdata('deleteadmin'); ?>
    <?php echo $this->session->flashdata('restoreadmin'); ?>
    <div class="card mb-3">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <i class="header-icon pe-7s-copy-file mr-3 text-muted opacity-6"></i> 
                Manage Admin
            </div>
            <div class="btn-actions-pane-right actions-icon-btn"></div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Email Admin</th>
                        <th>Nama Admin</th>
                        <th>Dibuat</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php $no = 1; foreach ($admin as $dataadmin) { ?>
                    <tr>
                        <td><?php echo $no++?></td>
                        <td><?php echo $dataadmin['email'];?></td>
                        <td><?php echo $dataadmin['nama'];?></td>
                        <td><time class="timeago" datetime="<?php echo $dataadmin['created_at']; ?>" title="<?php echo date("j F Y H:i:s", strtotime($dataadmin['updated_at'])); ?>"></td>
                        <td>
                            <div>
                                <div class="dropdown">
                                    <button type="button" class="btn btn-danger delete-button" data-toggle="modal" data-target="#exampleModaldeleteAdmin" data-base_url="<?php echo base_url('Manage_admin'); ?>" data-idadmin='<?php echo $dataadmin['id_admin']; ?>' data-admin='<?php echo $dataadmin['nama']; ?>'>   
                                        Hapus
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>    
</div>