<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>
                    Tambahkan Admin
                    <div class="page-title-subheading">Tambahkan Admin Baru Disini !
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Create Admin</h5>
                    <form action="<?php echo (base_url('Manage_admin/daftar_admin')); ?>" method="post" enctype="multipart/form-data">
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label for="exampleFile" class="text-center">Image</label>
                                    <input type="file" id="inputanFoto" accept="gambar/*" name="gambar" class="form-control-file dropify" required>
                                    <small class="form-text text-danger">* Jika Gambar yang kamu upload tidak muncul gambar terlalu besar ukurannya.</small>
                                    <small class="form-text text-danger">* Gambar harus kurang dari 2MB.</small>
                                    <small id="gambar-check2" class="text-danger">* Gambar Tidak Boleh Kosong</small>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <input id="emailValidation" placeholder="Email here..." type="email" name="email" class="form-control" required>
                                    <small id="email-check" class="text-danger">* Email Sudah Terdaftar</small>
                                    <small id="email-check2" class="text-danger">* Email Tidak Boleh Kosong</small>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <input name="nama" id="nama" placeholder="Name here..." type="text" class="form-control" required>
                                    <small id="nama-check2" class="text-danger">* Nama Tidak Boleh Kosong</small>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <input name="password" id="passwordmanageAdmin" placeholder="Password here..." type="password" class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                                    <div class="pass-hint">
                                    <small id="pass-char" class="text-danger">* Password minimal 8 karakter</small><br>
                                    <small id="pass-upper" class="text-danger">* Password harus memiliki huruf kapital</small><br>
                                    <small id="pass-lower" class="text-danger">* Password harus memiliki huruf kecil</small><br>
                                    <small id="pass-number" class="text-danger">* Password harus memiliki angka</small><br>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <input name="passwordrep" id="confirm_passwordmanageAdmin" onchange="validatePassword()" placeholder="Repeat Password here..." type="password" class="form-control" required>
                                    <small id="pacword-check2" class="text-danger">* Password Tidak Sama</small>
                                </div>
                            </div>
                        </div>
                        <a class="mt-1 btn btn-primary text-light" onclick="myFunction()">Submit</a>
                        <button type="button" onclick="this.form.submit()" class="btn_send" style="display: none;"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    HideTag();
    var fileVal = document.getElementById("inputanFoto");
    var passInput = document.getElementById("passwordmanageAdmin");
    var lower = document.getElementById("pass-lower");
    var capital = document.getElementById("pass-upper");
    var number = document.getElementById("pass-number");
    var length = document.getElementById("pass-char");
    var confirm_password = document.getElementById("confirm_passwordmanageAdmin");
    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    var numbers = /[0-9]/g;

    function HideTag() {
        $('#email-check').hide();
        $('#email-check2').hide();
        $('#nama-check2').hide();
        $('#gambar-check2').hide();
        $('#pacword-check2').hide();
    }

    function myFunction(){
        HideTag();
        if (fileVal.value == "") {
            $('#gambar-check2').show();
        }else if ($('#emailValidation').val() ==  " " || $('#emailValidation').val() ==  "") {
            $('#email-check2').show();
        }else if ($('#nama').val() ==  " " || $('#nama').val() ==  "") {
            $('#nama-check2').show();
        }else if(passInput.value != confirm_password.value) {
            $('#pacword-check2').show();
        }else if(passInput.value.match(lowerCaseLetters) && passInput.value.match(upperCaseLetters) && passInput.value.match(numbers) &&  passInput.value.length >= 8){
            HideTag();
                $.ajax({
                    url:"<?php echo base_url(); ?>Manage_admin/checkDataByEmail",
                    method:"POST",
                    data:{email: $('#emailValidation').val()},
                    success:function(data)
                    {
                        console.log(data);
                        if(data != "")
                        {
                            $('#email-check').show();
                        }
                        else
                        {
                            $('.btn_send').trigger('click');
                        }
                    }
                })
        }else{
            alert("Tolong Masukkan Password Sesuai Aturan");
        }
    }
</script>
<script type="text/javascript">

        // function validatePassword(){
        //   if(password.value != confirm_password.value) {
        //     confirm_password.setCustomValidity("Passwords Don't Match");
        //   } else {
        //     confirm_password.setCustomValidity('');
        //   }
        // }

        // password.onchange = validatePassword;
        // confirm_password.onkeyup = validatePassword;
</script>
<script>
    var passInput = document.getElementById("passwordmanageAdmin");
    var lower = document.getElementById("pass-lower");
    var capital = document.getElementById("pass-upper");
    var number = document.getElementById("pass-number");
    var length = document.getElementById("pass-char");

    passInput.onkeyup = function() {
        var lowerCaseLetters = /[a-z]/g;
        if(passInput.value.match(lowerCaseLetters)) { 
            lower.classList.remove("text-danger");
            lower.classList.add("text-success");
        } else {
            lower.classList.remove("text-success");
            lower.classList.add("text-danger");
        }

        var upperCaseLetters = /[A-Z]/g;
        if(passInput.value.match(upperCaseLetters)) { 
            capital.classList.remove("text-danger");
            capital.classList.add("text-success");
        } else {
            capital.classList.remove("text-success");
            capital.classList.add("text-danger");
        }

        var numbers = /[0-9]/g;
        if(passInput.value.match(numbers)) { 
            number.classList.remove("text-danger");
            number.classList.add("text-success");
        } else {
            number.classList.remove("text-success");
            number.classList.add("text-danger");
        }

        if(passInput.value.length >= 8) {
            length.classList.remove("text-danger");
            length.classList.add("text-success");
        } else {
            length.classList.remove("text-success");
            length.classList.add("text-danger");
        }

    }
</script>