<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-note icon-gradient bg-happy-itmeo">
                    </i>
                </div>
                <div>Update Kategori
                    <div class="page-title-subheading">Perbarui Kategori Kesukaanmu Disini.
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Update Kategori</h5>
                    <form action="<?php echo base_url('manage_kategori/edit/'.$kategori->id_kategori); ?>" method="post" enctype="multipart/form-data">
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class="">Judul</label>
                            <input name="kategori" placeholder="" type="text" class="form-control" id="judul" value="<?php echo ($kategori->kategori) ?>" onkeyup="createslug()" required>
                        </div>
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class="">Slug</label>
                            <input name="slug" placeholder="" type="text" class="form-control" id="slug" value="<?php echo ($kategori->slug) ?>" readonly>
                        </div>
                        <div class="modal-footer clearfix">
                            <div class="float-right">
                                <button type="button" class="btn btn-primary btn-lg" onclick="checkData()">Submit</button>
                                <button type="button" onclick="this.form.submit()" class="btn_send" style="display: none;"></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkData(){
        if ($('#judul').val() != "" && $('#slug').val() != "") {
            $('.btn_send').trigger('click');
        } else {
            alert("Please Fill Out All Form !");
            return;
        }
    }
</script>