<div class="app-main__inner">
    <div class="row">
        <div class="col-md-3 col-xl-4">
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Artikel</div>
                        <div class="widget-subheading">Total Artikel yang Ada</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $ttl_artikel;?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xl-4">
            <div class="card mb-3 widget-content bg-arielle-smile">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Santri</div>
                        <div class="widget-subheading">Total Santri yang Sudah Register</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $ttl_user;?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xl-4">
            <div class="card mb-3 widget-content bg-grow-early">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Santri Daftar</div>
                        <div class="widget-subheading">Total Santri yang Sudah Daftar</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $ttl_user_daftar; ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xl-4">
            <div class="card mb-3 widget-content bg-plum-plate">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Ustadz</div>
                        <div class="widget-subheading">Total Ustadz yang Ada</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $ttl_ustadz; ?></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">
                    Nama Santri Yang Mendaftar
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Nama</th>
                            <th>Tanggal Daftar</th>
                            <th>Tempat, Tanggal Lahir</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no= 1; foreach ($santri as $dataUser) { ?>
                            <tr>
                                <td class="text-center text-muted">#<?php echo $no++; ?></td>
                                <td><?php echo $dataUser['nama']; ?></td>
                                <td>
                                    <?php echo date('d', strtotime($dataUser['created_at'])); ?> 
                                    <?php 
                                        $Bulan = array("January", "February", "Maret", "April", "May", "June", "July", "August", "September", "October", "November", "Desember",);
                                        echo $Bulan[date('m', strtotime($dataUser['created_at'])) - 1]; 
                                    ?> 
                                    <?php echo date('Y', strtotime($dataUser['created_at'])); ?> 
                                </td>
                                <td><?php echo $dataUser['tempat_lahir'];?>, <?php echo $dataUser['tanggal_lahir'];?></td>
                                 <td>
                                    <?php if ($dataUser['status'] == 0) { ?>
                                        <div class="mb-2 mr-2 badge badge-warning" style="color: white;">Pending</div>
                                    <?php } ?>
                                    <?php if ($dataUser['status'] == 1) { ?>
                                        <div class="mb-2 mr-2 badge badge-success">Diterima</div>
                                    <?php } ?>
                                    <?php if ($dataUser['status'] == 2) { ?>
                                        <div class="mb-2 mr-2 badge badge-danger">Ditolak</div>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
                <div class="d-block text-center card-footer"></div>
            </div>
        </div>
    </div>    
</div> 