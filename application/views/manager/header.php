<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>
        <?php 
            if($page == "dashboard"){ echo "Dashboard";}
            if($page == "artikel"){ echo "Manage Artikel";}
            if($page == "kategori"){ echo "Manage Kategori";}
            if($page == "santri"){ echo "Manage Santri";} 
            if($page == "ustadz"){ echo "Manage Ustadz";} 
            if($page == "santriRequest"){ echo "Manage Pendaftaran Santri";} 
            if($page == "admin"){ echo "Manage Admin";} 
        ?> - Pondok Pesantren Al-Islam</title>
    <meta name="viewport" content="width=device-width, initial-scale=0.5">
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <style type="text/css">
        .block_Page{
            display: none;
        }
        .block_Page2{
            display: block;
        }
        @media only screen and (min-width: 310px) and (max-width: 562px) and (-webkit-min-device-pixel-ratio: 1){
            .block_Page{
                width: 100%;
                position: absolute;
                display: block;
                height: auto;
                background-color: white;
                color: black;
                z-index: 1;
                margin-top: 100%;
                text-align: center;
            }
            .block_Page2{
                display: none;
            }
        }
    </style>
    <link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dropify/dropify.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/Croppie/croppie.css'?>">

    <script type="text/javascript" src="<?php echo base_url('assets/scripts/main.87c0748b313a1dda75f5.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/Croppie/jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/Croppie/croppie.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/Croppie/bootstrap.min.js'?>"></script>
    </head>
<body>
    <div class="block_Page">
        <p>
            Maaf anda tidak bisa mengakses halaman ini di handphone.
            Miringkan Handphone anda atau gunakan web chrome dan aktifkan mode dekstop.
            Terima Kasih.
        </p>
    </div>
    <div class="block_Page2">
        <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src" style="width: 120px;"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="<?php echo base_url($data_admin['gambar']);?>" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                            <div class="dropdown-menu-header">
                                                <div class="dropdown-menu-header-inner bg-grow-early ">
                                                    <div class="menu-header-image opacity-2" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                                    <div class="menu-header-content text-left">
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left mr-3">
                                                                    <img width="42" class="rounded-circle"
                                                                         src="<?php echo base_url($data_admin['gambar']);?>"
                                                                         alt="">
                                                                </div>
                                                                <div class="widget-content-left">
                                                                    <div class="widget-heading"><?php echo ($data_admin['nama']);?>
                                                                    </div>
                                                                    <div class="widget-subheading opacity-8"><?php echo ($data_admin['email']);?>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-right mr-2">
                                                                    <form action="<?php echo base_url('login/logout');?>" method="post" accept-charset="utf-8">
                                                                        <button class="btn-pill btn-shadow btn-shine btn btn-focus">Logout
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="scroll-area-xs" style="height: 250px;">
                                                <div class="scrollbar-container ps">
                                                    <div class="card-body">
                                                        <form action="<?php echo base_url('Dashboard/profilEdit/'.$this->session->userdata('id_admin')); ?>" method="post" enctype="multipart/form-data">
                                                            <div class="position-relative form-group" id="masukkan_admin">
                                                                <label for="exampleFile" class="text-center">Image</label>
                                                                <input type="file" accept="gambar/*" name="gambar" class="form-control-file dropify text-center" id="inputanFotoHeader">
                                                                <small class="form-text text-danger">Jika Gambar yang kamu upload tidak muncul gambar terlalu besar ukurannya.</small>
                                                                <small class="form-text text-danger">Gambar harus kurang dari 2MB.</small>
                                                                <small id="gambar-ngecheck2" class="text-danger">* Gambar Tidak Boleh Kosong</small>
                                                                <input type="button" class="btn btn-danger cancel_admin" value="Cancel"/>
                                                            </div>
                                                            <div class="position-relative form-group text-center" id="tampilan_admin">
                                                                <label for="exampleFile" class="">Image</label>
                                                                <div class="col-md-12 mb-2 p-0">
                                                                    <img src='<?php echo base_url($data_admin['gambar']); ?>' style="max-width: 35%;">
                                                                </div>
                                                                <input type="button" class="btn btn-success change_admin" value="Change"/>
                                                            </div>
                                                            <script>
                                                                $('#masukkan_admin').hide();             
                                                                $('.cancel_admin').hide();   
                                                                          
                                                                $('.cancel_admin').click(function(){
                                                                    $('#masukkan_admin').hide();
                                                                    $('#tampilan_admin').show();
                                                                    $('.change_admin').show();
                                                                    $('.cancel_admin').hide(); 
                                                                });

                                                                $('.change_admin').click(function(){
                                                                    $('#masukkan_admin').show();
                                                                    $('#tampilan_admin').hide();     
                                                                    $('.cancel_admin').show(); 
                                                                    $('.change_admin').hide();  
                                                                });
                                                            </script>
                                                            <div class="position-relative form-group">
                                                                <label for="exampleEmail" class="">Nama</label>
                                                                <input name="nama" placeholder="" id="namaHeader" type="text" class="form-control" value="<?php echo ($data_admin['nama']) ?>">
                                                                <input type="hidden" placeholder="" id="namaHeadercheck" value="<?php echo ($data_admin['nama']) ?>" style="display: none;">
                                                                <small id="nama-ngecheck2" class="text-danger">* Nama Tidak Boleh Kosong</small>
                                                                <input name="page" placeholder="" type="hidden" class="form-control" value="<?php echo ($page) ?>">
                                                            </div>
                                                            <div class="position-relative form-group">
                                                                <label for="exampleEmail" class="">Email</label>
                                                                <input name="email" placeholder="" id="emailHeader" type="email" class="form-control" value="<?php echo ($data_admin['email']) ?>" required>
                                                                <input type="hidden" placeholder="" id="emailHeadercheck" value="<?php echo ($data_admin['email']) ?>" style="display: none;">
                                                                <small id="email-ngecheck" class="text-danger">* Email Sudah Terdaftar</small>
                                                            </div>
                                                            <div class="position-relative form-group">
                                                                <label for="exampleEmail" class="">Password</label>
                                                                <input name="password" id="passwordHeader" placeholder="" type="password" class="form-control">
                                                                <small class="form-text text-danger">* Password Tidak Harus Diisi.</small>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="nav flex-column">
                                                <li class="nav-item-divider nav-item">
                                                </li>
                                                <li class="nav-item-btn text-center nav-item">
                                                    <a class="btn-wide btn btn-primary btn-sm text-light" onclick="myFunctionHeader()">
                                                        Edit Profile
                                                    </a>                                                    
                                                    <button type="button" onclick="this.form.submit()" class="btn_sendHeader" style="display: none;"></button>
                                                    <?php 
                                                        if ($data_admin['level'] == 1) {
                                                            ?>
                                                            <a href="<?php echo base_url('Dashboard/deleteAkun/'.$data_admin['id_admin']); ?>" class="btn-wide btn btn-danger btn-sm">
                                                                Delete Account
                                                            </a>
                                                            <?php
                                                        }
                                                    ?>
                                                </li>
                                            </ul>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading">
                                        <?php echo $data_admin['nama'];?>
                                    </div>
                                    <div class="widget-subheading">
                                        <?php echo $data_admin['email'];?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>        
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow bg-dark sidebar-text-light">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>    
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            <li class="app-sidebar__heading">Dashboards</li>
                            <li>
                                <a href="<?php echo base_url('Dashboard')?>" class="<?php if($page == "dashboard"){ echo "mm-active";} ?>">
                                    <i class="metismenu-icon pe-7s-rocket"></i>
                                    Dashboard
                                </a>
                            </li>                                
                        </ul>
                        <ul class="vertical-nav-menu">
                            <li class="app-sidebar__heading">Manage</li>
                            <li>
                                <a href="<?php echo base_url('Manage_artikel/')?>" class="<?php if($page == "artikel"){ echo "mm-active";} ?>">
                                    <i class="metismenu-icon pe-7s-albums"></i>
                                    Artikel
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('Manage_kategori/')?>" class="<?php if($page == "kategori"){ echo "mm-active";} ?>">
                                    <i class="metismenu-icon pe-7s-copy-file"></i>
                                    Kategori
                                </a>
                            </li>  
                            <li>
                                <a href="<?php echo base_url('Manage_ustadz/')?>" class="<?php if($page == "ustadz"){ echo "mm-active";} ?>">
                                    <i class="metismenu-icon fa fa-users"></i>
                                    Ustadz
                                </a>
                            </li> 
                            <li>
                                <a href="<?php echo base_url('Manage_santri/')?>" class="<?php if($page == "santri"){ echo "mm-active";} ?>">
                                    <i class="metismenu-icon pe-7s-users"></i>
                                    Santri
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('Manage_requestuser/')?>" class="<?php if($page == "santriRequest"){ echo "mm-active";} ?>">
                                    <i class="metismenu-icon pe-7s-add-user"></i>
                                    Pendaftaran Santri
                                </a>
                            </li>  
                        </ul>
                        <?php if ($this->session->userdata('level') == 0): ?>
                            <ul class="vertical-nav-menu">
                                <li class="app-sidebar__heading">Admin</li>
                                <li>
                                    <a href="<?php echo base_url('Manage_admin/')?>" class="<?php if($page == "admin"){ echo "mm-active";} ?>">
                                        <i class="metismenu-icon pe-7s-user"></i>
                                        Admin
                                    </a>
                                </li>                               
                            </ul>
                        <?php endif ?>
                    </div>
                </div>
            </div>    
            <div class="app-main__outer">