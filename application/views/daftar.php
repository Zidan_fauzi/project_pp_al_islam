<!doctype html>
<html lang="en">


<!-- Mirrored from demo.dashboardpack.com/architectui-html-pro/pages-login-boxed.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Nov 2019 12:34:34 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login Boxed - ArchitectUI HTML Bootstrap 4 Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dropify/dropify.css'?>">
</head>

<body>
<div class="app-container app-theme-white body-tabs-shadow">
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container" style="height: 210px">
            <div class="bg-plum-plate bg-animation">
                <div class="d-flex justify-content-center align-items-center">
                    <div class="mx-auto app-login-box col-md-8">
                        <div class="modal-dialog w-100 mx-auto">
                            <div class="modal-content">
                                    <div class="modal-body">
                                        <h5 class="modal-title">
                                            <h4 class="mt-2">
                                                <div>Silahkan Mendaftar Admin Disini,</div>
                                                <span>Harap Isi Form yang Sudah Disediakan</span>
                                            </h4>
                                        </h5>
                                        <div class="divider row"></div>
                                        <div>
                                            <form action="<?php echo base_url('Pendaftaran/daftar_admin') ?>" method="post" enctype="multipart/form-data">
                                            <div class="col-md-12">
                                                <div class="position-relative form-group">
                                                    <label for="exampleFile" class="text-center">Image</label>
                                                    <input type="file" accept="gambar/*" name="gambar" class="form-control-file dropify" required>
                                                    <small class="form-text text-danger">* Jika Gambar yang kamu upload tidak muncul gambar terlalu besar ukurannya.</small>
                                                    <small class="form-text text-danger">* Gambar harus kurang dari 2MB.</small>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="position-relative form-group"><input name="email" placeholder="Email here..." type="email" class="form-control" required></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="position-relative form-group"><input name="nama" placeholder="Name here..." type="text" class="form-control" required></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="position-relative form-group">
                                                    <input name="password" id="password" placeholder="Password here..." type="password" class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                                                    <div class="pass-hint">
                                                    <small id="pass-char" class="text-danger">* Password minimal 8 karakter</small><br>
                                                    <small id="pass-upper" class="text-danger">* Password harus memiliki huruf kapital</small><br>
                                                    <small id="pass-lower" class="text-danger">* Password harus memiliki huruf kecil</small><br>
                                                    <small id="pass-number" class="text-danger">* Password harus memiliki angka</small><br>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="position-relative form-group"><input name="passwordrep" id="confirm_password" placeholder="Repeat Password here..." type="password" class="form-control" required></div>
                                            </div>
                                        </div>
                                        <div class="divider row"></div>
                                        <h6 class="mb-0">Already have an account? <a href="<?php echo base_url('login') ?>" class="text-primary">Sign in</a></h6></div>
                                        <div class="modal-footer d-block text-center">
                                            <button class="btn-wide btn-pill btn-shadow btn-hover-shine btn btn-primary btn-lg">Create Account</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<<script type="text/javascript" src="<?php echo base_url('assets/scripts/main.87c0748b313a1dda75f5.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/jquery.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/dropify/dropify.min.js'?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dropify').dropify({
            messages: {
                default: 'Drag atau drop untuk memilih gambar',
                replace: 'Ganti',
                remove:  'Hapus',
                error:   'error'
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var password = document.getElementById("password"), confirm_password = document.getElementById("confirm_password");

        function validatePassword(){
          if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
          } else {
            confirm_password.setCustomValidity('');
          }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    });
</script>
<script>
    var passInput = document.getElementById("password");
    var lower = document.getElementById("pass-lower");
    var capital = document.getElementById("pass-upper");
    var number = document.getElementById("pass-number");
    var length = document.getElementById("pass-char");

    passInput.onkeyup = function() {
        var lowerCaseLetters = /[a-z]/g;
        if(passInput.value.match(lowerCaseLetters)) { 
            lower.classList.remove("text-danger");
            lower.classList.add("text-success");
        } else {
            lower.classList.remove("text-success");
            lower.classList.add("text-danger");
        }

        var upperCaseLetters = /[A-Z]/g;
        if(passInput.value.match(upperCaseLetters)) { 
            capital.classList.remove("text-danger");
            capital.classList.add("text-success");
        } else {
            capital.classList.remove("text-success");
            capital.classList.add("text-danger");
        }

        var numbers = /[0-9]/g;
        if(passInput.value.match(numbers)) { 
            number.classList.remove("text-danger");
            number.classList.add("text-success");
        } else {
            number.classList.remove("text-success");
            number.classList.add("text-danger");
        }

        if(passInput.value.length >= 8) {
            length.classList.remove("text-danger");
            length.classList.add("text-success");
        } else {
            length.classList.remove("text-success");
            length.classList.add("text-danger");
        }

    }
</script>
</body>
<!-- Mirrored from demo.dashboardpack.com/architectui-html-pro/pages-login-boxed.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Nov 2019 12:34:34 GMT -->
</html>
