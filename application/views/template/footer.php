<footer class="footer-section bg-white">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <h3>Tentang Pondok Pesantren</h3>
            <p>Pondok Pesantren Al-Islam didirikan oleh KH. Imam Asfali sejak tahun 1952. Bertempat di Kota Malang.</p>
          </div>

          <div class="col-md-3 ml-auto">
            <h3>Links</h3>
            <ul class="list-unstyled footer-links">
              <li><a href="<?=base_url('home')?>#home-section">Beranda</a></li>
              <li><a href="<?=base_url('home')?>#courses-section">Artikel</a></li>
              <li><a href="<?=base_url('home')?>#programs-section">Program</a></li>
              <li><a href="<?=base_url('home')?>#teachers-section">Ustadz</a></li>
            </ul>
          </div>

          <div class="col-md-4">
            <h3>Hubungi Kami</h3>
            <p>
              Nomor Handphone : 082244768008 / 081336947269<br>
              Alamat      : Jl. Prof Ahmad Yamin No.IV/20 Suharjo, Kec. Klojen, Malang<br>
            </p>
            
          </div>

        </div>

        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
            <p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p>
            </div>
          </div>
          
        </div>
      </div>
    </footer>