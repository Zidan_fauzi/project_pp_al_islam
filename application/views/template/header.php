<header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">
      
      <div class="container-fluid">
        <div class="d-flex align-items-center">
          <div class="site-logo mr-auto col-lg-3 col-sm-6"><a href="<?=base_url('home')?>">PP Al-Islam</a></div>

          <div class="mx-auto text-center">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mx-auto d-none d-lg-block  m-0 p-0">
                
                <?php
                  $home = '<li><a href="#home-section" class="nav-link">Beranda</a></li>';
                  $title = '<li><a href="#home-section" class="nav-link">Title</a></li>';
                  $cource = '<li><a href="#courses-section" class="nav-link">Artikel</a></li>';
                  $program = '<li><a href="#programs-section" class="nav-link">Program</a></li>';
                  $teachers ='<li><a href="#teachers-section" class="nav-link">Ustadz</a></li>';
                  echo ($page == "home") ? $home : '';
                  echo ($page == "home") ? $cource  : '';
                  echo ($page == "home") ? $program : '';
                  echo ($page == "home") ? $teachers : '';
                ?>
              </ul>
            </nav>
          </div>

          <div class="ml-auto w-25">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu site-menu-dark js-clone-nav mr-auto d-none d-lg-block m-0 p-0">
                
              </ul>
            </nav>
            <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-white float-right"><span class="icon-menu h3"></span></a>
          </div>
        </div>
      </div>
      
    </header>