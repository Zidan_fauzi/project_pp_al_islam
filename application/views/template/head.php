	<title>PP Al-Islam &mdash; Website by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/jquery-ui.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/aos.css">

    <link rel="stylesheet" href="<?=base_url();?>assets/frontpage/css/style.css">