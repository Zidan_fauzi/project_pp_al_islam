	<script src="<?=base_url();?>assets/frontpage/js/jquery-3.3.1.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/jquery-ui.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/popper.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/bootstrap.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/owl.carousel.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/jquery.stellar.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/jquery.countdown.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/bootstrap-datepicker.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/jquery.easing.1.3.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/aos.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/jquery.fancybox.min.js"></script>
  <script src="<?=base_url();?>assets/frontpage/js/jquery.sticky.js"></script>

  
  <script src="<?=base_url();?>assets/frontpage/js/main.js"></script>