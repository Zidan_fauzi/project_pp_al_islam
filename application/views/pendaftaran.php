<!doctype html>
<html lang="en">


<!-- Mirrored from demo.dashboardpack.com/architectui-html-pro/pages-login-boxed.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Nov 2019 12:34:34 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Pendaftaran - Pondok Pesantren Al-Islam</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="<?php echo base_url('assets/css/main.87c0748b313a1dda75f5.css')?>" rel="stylesheet"></head>

    <script type="text/javascript" src="<?php echo base_url('assets/scripts/main.87c0748b313a1dda75f5.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/scripts/jquery.min.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/dropify/dropify.css'?>">

<body>
<div class="app-container app-theme-white body-tabs-shadow">
    <div class="app-container">
        <div class="bg-grow-early bg-animation" style="padding-top:35px; padding-bottom: 30px;">
            <div class="d-flex justify-content-center align-items-center">
                <div class="mx-auto app-login-box col-md-12">
                    <div class="w-100">
                        <div class="modal-content col-lg-6 col-md-12 col-sm-12" style="margin : 0 auto">
                            <div class="modal-body">
                                <div class="h5 modal-title text-center">
                                    <h4 class="mt-2">
                                        <div>Selamat Datang Santri Baru,</div>
                                        <span>Silahkan Mendaftar di Form Yang Disediakan.</span>
                                    </h4>
                                </div>
                                <form action="<?php echo base_url().'Pendaftaran/daftar' ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label for="">Nama Lengkap <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <input name="nama" id="nama" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Jenis Kelamin <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <select name="JenisKelamin" id="JenisK" class="form-control">
                                                    <option value="1"> Laki-Laki</option>
                                                    <option value="2"> Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Tempat Lahir <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <input name="tempat_lahir" id="tempat" type="text" class="form-control" required>
                                            </div>                                  
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Tanggal Lahir <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <input type="date" id="tanggal" class="form-control" name="tanggal_lahir" required>
                                            </div>  
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Nama Orangtua <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <input name="nama_ortu" id="namaOrtu" type="text" class="form-control" required>
                                            </div>                                  
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Nomor Handphone <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <input name="no_HP" id="noTelp" type="tel" class="form-control" required>
                                            </div>                                  
                                        </div>
                                        <div class="col-md-12">
                                            <label for="">Alamat <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <textarea name="alamat" id="alamat" type="text" class="form-control" required></textarea>
                                            </div>                                        
                                        </div>
                                        <div class="col-md-12">
                                            <label for="">Foto Anda <strong class="text-danger">*</strong></label>
                                            <div class="position-relative form-group">
                                                <input type="file" accept="gambar_santri/*" id="inputanFoto" name="gambar_santri" class="form-control-file dropify" required data-height="200" required>
                                            </div>
                                            <label class="form-text text-danger">* Jika Gambar yang kamu upload tidak muncul, gambar terlalu besar ukurannya.</label>
                                            <label class="form-text text-danger">* Gambar harus kurang dari 2MB dan berukuran 4x6.</label>
                                        </div>
                                    </div>
                                    <div class="modal-footer clearfix">
                                        <div class="float-right">
                                            <div class="btn btn-primary btn-lg" data-toggle="modal" data-target="#exampleModal">Daftar</div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Submit Data</h5>
                                                    <a class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </a>
                                                </div>
                                                <div class="modal-body">
                                                    <p id="modalDeleteJudul" class="mb-0">Apakah Kamu Yakin Mengisi Data Dengan Benar ?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary btn-lg" onclick="checkData()">Kirim Data</button>
                                                    <button type="button" onclick="this.form.submit()" class="btn_send" style="display: none;"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/dropify/dropify.min.js'?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dropify').dropify({
            messages: {
                default: 'Upload Fotomu Disini, Drag and Drop',
                replace: 'Ganti',
                remove:  'Hapus',
                error:   'error'
            }
        });
    });
</script>
<script type="text/javascript">
    function checkData(){
        var fileVal = document.getElementById("inputanFoto");
        console.log(fileVal.value);
        if ($('#nama').val() != "" && $('#jenisK').val() != "" && $('#tempat').val() != "" && $('#tanggal').val() != "" && $('#namaOrtu').val() != "" && $('#noTelp').val() != "" && $('#alamat').val() != "" && fileVal.value != "") {
            $('.btn_send').trigger('click');
        } else {
            alert("Please Fill Out All Form !");
            return;
        }
    }
</script>
</body>

</html>
