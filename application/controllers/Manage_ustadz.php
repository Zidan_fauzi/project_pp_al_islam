<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_ustadz extends CI_Controller {

        function __construct(){

            parent::__construct();
            $this->load->model('M_admin');
            $this->load->model('M_ustadz');
            $this->load->model('M_dashboard');
            $this->load->helper('url', 'time_ago');
            $this->load->helper(array('string', 'text'));
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->library('pagination');
            if ($this->session->userdata('id_admin') == null) {
                redirect('Home');
            }
        }

        public function index()
        {
            $per_page = 1000;
            $page = 1000;
            $data['admin'] = $this->M_ustadz->getAll(); 
            // var_dump($data['admin']);exit();
            $data['data_admin'] = $this->M_dashboard->getUser();        

            $data['page'] = 'ustadz';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/ustadz/manage_ustadz', $data);
            $this->load->view('manager/footer');
        }

        public function upload_foto(){
          $data = $_POST['images'];
          list($type, $data) = explode(';', $data);
          list(, $data)      = explode(',', $data);
          $data = base64_decode($data);
          $imageName = time().'.png';
          file_put_contents('gambar_profile/'.$imageName, $data);
          // $data['gambar'] = $imageName;
          // redirect('manager/template_admin/v_register', $data);
          echo '<img src="'.base_url().'/gambar_profile/'.$imageName.'" name="gambar_profile" id="img-profile" />
          <input type="hidden" name="gambar_profile" value="gambar_profile/'.$imageName.'">
          ';
        }

        public function indexCreate()
        {
            $data["page"] = "ustadz";
            $data['data_admin'] = $this->M_dashboard->getUser(); 
            $this->load->view('manager/header', $data);
            $this->load->view('manager/ustadz/create_ustadz', $data);
            $this->load->view('manager/footer');           
        }

        public function indexUpdate($id)
        {
            $data["page"] = "ustadz";
            $data['Data'] = $this->M_ustadz->getAllById($id);
            // var_dump($data['Data']);
            $data['data_admin'] = $this->M_dashboard->getUser(); 
            $this->load->view('manager/header', $data);
            $this->load->view('manager/ustadz/update_ustadz', $data);
            $this->load->view('manager/footer');           
        }

        public function create()
        {
                $data = array(
                    'nama_ustadz' => $_POST["namaUstadz"], 
                    'quotes' => $_POST["quotesUstadz"],
                    'foto_ustadz' => $_POST['gambar_profile'],
                    'status' => '0',
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'is_delete' => 0
                );
                $id = $this->M_ustadz->uploadUstadz($data);
                $this->session->set_flashdata('create', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Menambahkan Data Ustadz Dengan Namma
                        <a href="'.base_url(''.$this->db->insert_id()).'" class="alert-link">'.$_POST["namaUstadz"].'</a>.
                </div>');

               $this->index();
        }

        public function update($id)
        {
            if (!empty($_POST['gambar_profile'])) {
                $data = array(
                    'nama_ustadz' => $_POST["namaUstadz"], 
                    'quotes' => $_POST["quotesUstadz"],
                    'foto_ustadz' => $_POST['gambar_profile'],
                    'status' => '0',
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'is_delete' => 0
                );
            }else{
                $data = array(
                    'nama_ustadz' => $_POST["namaUstadz"], 
                    'quotes' => $_POST["quotesUstadz"],
                    'status' => '0',
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'is_delete' => 0
                );
            }
            $id = $this->M_ustadz->updateUstadz($data, $id);
            $this->session->set_flashdata('create', '<div class="alert alert-success fade show" role="alert">
                Kamu Berhasil Merbarui Data Ustadz Dengan Namma
                    <a href="'.base_url(''.$this->db->insert_id()).'" class="alert-link">'.$_POST["namaUstadz"].'</a>.
            </div>');

           $this->index(); 
        }

        public function delete($id)
        {
            if (!isset($id)) redirect('');
            if ($this->M_ustadz->delete($id)) {
                $data["admin"]= $this->M_ustadz->getByIdDelete($id);
                $this->session->set_flashdata('deleteadmin', '<div class="alert alert-danger fade show" role="alert">
                    Kamu Menghapus Ustadz Dengan Nama '.$data["admin"]->nama_ustadz.'. Apakah Kamu Ingin Mengembalikan Ustadz Dengan Nama '.$data["admin"]->nama_ustadz.' ? <a href="'.base_url('Manage_ustadz/restore/'.$id).'" class="alert-link">Kembalikan Ustadz <i class="fa fa-fw" aria-hidden="true" title="Copy to use rotate-left"></i></a>. 
                </div>');
                $this->index();
            }
        }

        public function restore($id)
        {
            if ($this->M_ustadz->restore($id)) {
                $data["admin"]= $this->M_ustadz->getByIdDelete($id);
                $this->session->set_flashdata('deleteadmin', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Mengembalikan Admin Dengan Nama'.$data["admin"]->nama_ustadz.'.
                </div>');
                
                $this->index();
            }
        }
        
    }
?>