<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

        function __construct(){

                parent::__construct();

                $this->load->model('m_artikel');
                $this->load->model('m_kategori');
                $this->load->helper('url', 'time_ago');
                $this->load->helper(array('string', 'text'));
                $this->load->library('form_validation');
                $this->load->library('session');
                $this->load->library('pagination');
        }

        public function index()
        {
            $data['page'] = "Contact";
            $data['css'] = '<link rel="stylesheet" type="text/css" href="'.base_url().'assets_Guest/styles/contact.css">
            <link rel="stylesheet" type="text/css" href="'.base_url().'assets_Guest/styles/contact_responsive.css">';

            $data['js'] = '<script src="'.base_url().'assets_Guest/plugins/parallax-js-master/parallax.min.js"></script><script src="'.base_url().'assets_Guest/js/contact.js"></script>';
            
            $this->load->view('guest/header', $data);
            $this->load->view('guest/contact', $data);
            $this->load->view('guest/footer', $data);
        }
    }
?>