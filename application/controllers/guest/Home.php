<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

        function __construct(){

                parent::__construct();

                $this->load->model('m_artikel');
                $this->load->model('m_kategori');
                $this->load->helper('url', 'time_ago');
                $this->load->helper(array('string', 'text'));
                $this->load->library('form_validation');
                $this->load->library('session');
                $this->load->library('pagination');
        }

        public function index()
        {
            $data['page'] = "Home";
            $data['css'] = '<link rel="stylesheet" type="text/css" href="'.base_url().'assets_Guest/styles/main_styles.css"><link rel="stylesheet" type="text/css" href="'.base_url().'assets_Guest/styles/responsive.css">';
            $data['js'] = '<script src="'.base_url().'assets_Guest/plugins/masonry/images_loaded.js"></script><script src="'.base_url().'assets_Guest/js/custom.js"></script>';
            $data['recomended_article'] = $this->m_artikel->artikel_rekomended();
            $data['latest_article3'] = $this->m_artikel->latest_article3();
            $data['article_all'] = $this->m_artikel->getAll();
            $data['latest_article_all'] = $this->m_artikel->latest_article_all();
            $data['oldest_article_all'] = $this->m_artikel->oldest_article_all();
            $data['kategori'] = $this->m_kategori->getAll();
            
            $this->load->view('guest/header', $data);
            $this->load->view('guest/landing_page', $data);
            $this->load->view('guest/footer', $data);
        }
        
        public function detail($slug)
        {
            $data['page'] = "Home";
            $data['css'] = '<link rel="stylesheet" type="text/css" href="'.base_url().'assets_Guest/styles/post_nosidebar.css"><link rel="stylesheet" type="text/css" href="'.base_url().'assets_Guest/styles/post_nosidebar_responsive.css">';
            $data['js'] = '<script src="'.base_url().'assets_Guest/plugins/parallax-js-master/parallax.min.js"></script><script src="'.base_url().'assets_Guest/js/post_nosidebar.js"></script>';
            
            $data['article_detail'] = $this->m_artikel->detail($slug);
            $data['article_related'] = $this->m_artikel->article_related($data['article_detail']['kategori']);
            // var_dump($data['article_related']);exit();
            $this->load->view('guest/header', $data);
            $this->load->view('guest/post_nosidebar', $data);
            $this->load->view('guest/footer', $data);
        }

        public function fetch()
        {
            $output = '';
            $artikel = $this->m_artikel->fetch_data($this->input->post('limit'), $this->input->post('start'));
            $total= count($artikel);
            if($this->m_artikel->fetch_jumlah_data() > 0){
                $output .= '<div class="grid clearfix">';
                foreach($artikel as $valuelate){
                    $output .= '
                        <div class="card card_small_with_image grid-item" style="margin-right: 29px;">
                            <div style="display: flex; justify-content: center; align-items: center; overflow: hidden; height: 180px;">
                                <img class="card-img-top" style="flex-shrink: 0; min-width: 100%; min-height: 100%;" src="'.base_url($valuelate['gambar']).'" alt="">
                            </div>
                            <div class="card-body">
                                <div class="card-title card-title-small"  style="height: 65px; overflow: hidden;">
                                    <a href="'.base_url('guest/Home/detail/'.$valuelate['slug']).'">
                                        '.$valuelate['nama_artikel'].'
                                    </a>
                                </div>
                                <small class="post_meta">
                                    <a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span>
                                </small>
                            </div>
                        </div>
                    ';
                }
                $output .= '<div class="card card_default card_default_with_background grid-item"></div>
                </div>';
            }else{
                $total= 0;
            }
            $data = array('hasil' => $output, 'total_data' => $total);
            echo json_encode($data);
        }
    }
?>