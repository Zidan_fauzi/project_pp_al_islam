<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        //load library form validasi
        $this->load->library('session');
        $this->load->library('form_validation');
        //load model admin
        $this->load->model('M_login');
        $this->check_cookie();
    }

    public function index()
    {
        if($this->session->userdata('email') != ''){
            redirect('dashboard');
        }else{
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            
            $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
                <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
            if ($this->form_validation->run() == TRUE) {
                $pengacak = "p3ng4c4k";

                $email = $this->input->post("email", TRUE);
                $remember = $this->input->post('remember');
                $password = MD5($this->input->post('password'));
                $password = md5($pengacak . md5($password));
                $checking = $this->M_login->check_login('tb_admin', array('email' => $email, 'is_delete' => 0, 'password' => $password));
                if ($checking != FALSE) {
                    foreach ($checking as $apps) {
                        // var_dump($apps->is_suspend);exit();
                        if($apps->is_delete != "1"){
                            if ($remember) {
                                $key = random_string('alnum', 64);
                                set_cookie('projectPKK', $key, 3600*24*30); 
                            
                                $update_data = array(
                                    'cookie' => $key
                                );
                                $this->M_login->update($update_data, $apps->id_admin);
                                $this->login($apps);
                            }else{
                                $this->login($apps);
                            }
                        }
                        else{
                            $data['error'] = '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fa fa-ban"></i> Maaf Akun anda sudah di blokir oleh admin. Silahkan Hubungi admin !</h5>
                            </div>';
                            // var_dump($data);
                            $this->load->view('login', $data);
                        }
                    }
                }
                else{
                    // exit;
                    $data['error'] = '
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top: 20px;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5>Sorry Your Email and Password Not Match.</h5>
                    </div>';
                    // var_dump($data);
                    $this->load->view('login', $data);
                }

            }
            else{
                $this->load->view('login');
            }
        }
    }
    
    private function check_cookie()
    {
        $cookie = get_cookie('projectPKK');

        if($cookie <> '') {
            // cek cookie
            $row = $this->M_login->get_by_cookie($cookie)->row();
            if ($row) {
                $this->login($row);
                $this->_daftarkan_session($row);
            }
        }
    }

    public function login($data)
    {
        $session_data = array(                          
            'id_admin' => $data->id_admin,
            'email' => $data->email,
            'nama' => $data->nama,
            'created_at' => $data->created_at,
            'gambar' => $data->gambar,
            'password' => $data->password,                          
            'level'   => $data->level
        );
        // var_dump($session_data);exit();
        if($data->level == 0 || $data->level == 1) {
            $this->M_login->logged_in($data->id_admin, $key);
            $this->session->set_userdata($session_data);
            redirect('dashboard/');
        }
        if($data->level == 2) {
            $this->M_login->logged_in($data->id_admin);
            $this->session->set_userdata($session_data);
            $this->not_admin();
        }
    }

    public function logout()
    {
      $id = $this->session->userdata('id_admin');
      $update_data = array(
        'cookie' => ''
      );
      $this->M_login->update($update_data, $id);
      $this->session->unset_userdata('id_admin');
      delete_cookie('projectPKK');
      $this->session->sess_destroy();
      redirect(base_url('Login'));
    }

    public function not_admin()
    {
        $this->load->view('not_admin');
    }
}
