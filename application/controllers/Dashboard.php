<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){

	    parent::__construct();

	    $this->load->model('M_dashboard');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        if ($this->session->userdata('id_admin') == null) {
            redirect('Home');
        }
	}
	public function index()
	{
        $data['ttl_user'] = count($this->M_dashboard->count_santri());
        $data['ttl_ustadz'] = count($this->M_dashboard->count_ustadz());
		$data['ttl_user_daftar'] = count($this->M_dashboard->count_santri_daftar());
		$data['ttl_artikel'] = count($this->M_dashboard->count_artikel());
		$data['santri'] = $this->M_dashboard->get_santriAll();
		$data['data_admin'] = $this->M_dashboard->getUser();

		$data['page'] = "dashboard";
		$this->load->view('manager/header', $data);
		$this->load->view('manager/dashboard', $data);
		$this->load->view('manager/footer');
	}

	public function profilEdit($id)
    {
        $pengacak = "p3ng4c4k";
        $password = MD5($this->input->post('password'));
        $password = md5($pengacak . md5($password));

        $config['upload_path'] = './gambar_profile/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $nmfile = "file_".time();
        $config['file_name'] = $nmfile;
        $this->load->library('upload', $config);        
        if($this->upload->do_upload('gambar')){
            $post = $this->input->post();
            $upload_data = $this->upload->data();
            $file_name = 'gambar_profile/'.$upload_data['file_name'];
        }

        if ($file_name == null) {
            if (!empty($this->input->post('password'))) {
                $data_edit = array(             
                    'email' => $_POST['email'],
                    'nama' => $_POST['nama'],
                    'password' => $password
                );
            } else {
                $data_edit = array(             
                    'email' => $_POST['email'],
                    'nama' => $_POST['nama']
                );
            }
        }else{
            if (!empty($this->input->post('password'))) {
                $data_edit = array(             
                    'email' => $_POST['email'],
                    'nama' => $_POST['nama'],
                    'gambar' => $file_name,
                    'password' => $password
                );
            } else {
                $data_edit = array(             
                    'email' => $_POST['email'],
                    'nama' => $_POST['nama'],
                    'gambar' => $file_name
                );
            }
        }

        $this->M_dashboard->profil_edit($id, $data_edit);
        redirect('dashboard/');
    }

    public function profil_editByname($id)
    {
        $pengacak = "p3ng4c4k";
        $password1 = MD5($this->input->post('password'));
        $password = md5($pengacak . md5($password1));

        if (empty($_POST['nama'])) {
            if (!empty($this->input->post('password'))) {
                $data_edit = array(            
                    'password' => $password
                );
                $this->M_dashboard->profil_edit($id, $data_edit);
            }
        }else{
            if (!empty($this->input->post('password'))) {
                $data_edit = array(             
                    'nama' => $_POST['nama'],
                    'password' => $password
                );
                $this->M_dashboard->profil_edit($id, $data_edit);
            } else {
                $data_edit = array(             
                    'nama' => $_POST['nama']
                );
                $this->M_dashboard->profil_edit($id, $data_edit);
            }
        }

        redirect('dashboard/');
    }

    public function deleteAkun($id)
    {
        $this->M_dashboard->delete_akun($id);
        redirect('Login/logout');
    }
}
