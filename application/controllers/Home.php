<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){

	    parent::__construct();

	    $this->load->model('M_home');
	}
	public function index()
	{
		

		$data['artikel'] = $this->M_home->head();
		$data['ustadz'] = $this->M_home->sel_ustadz();

		$this->load->view('frontpage/v_home',$data);
		// $this->load->view('manager/dashboard', $data);
		// $this->load->view('manager/footer');
	}
}
