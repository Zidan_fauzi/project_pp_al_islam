<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_requestUser extends CI_Controller {

      function __construct(){

            parent::__construct();

            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->load->model('M_userrequest');
            $this->load->library('session');
            $this->load->library('pagination');
            $this->load->model('M_dashboard');
            if ($this->session->userdata('id_admin') == null) {
                redirect('Home');
            }
      }

        public function index(){
            $data['santri'] = $this->M_userrequest->getAll();
            $data['page'] = 'santriRequest';
            $data['pesan'] = '';
            $data['pesan_notfounddata'] = '';
            $data['data_admin'] = $this->M_dashboard->getUser();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/santri/manage_santriRequest', $data);
            $this->load->view('manager/footer');
        }

        // public function detail($id = null){
        //     $data['user']= $this->M_userrequest->getById($id);

        //     $main_content['conten'] = $this->load->view('manager/content_admin/user/v_actionrequest', $data, true);
        //     $main_content['section_menu'] = $this->menu;
        //     $main_content['page'] = 'user';
        //     $this->load->view('manager/template_admin/header_footer', $main_content);
        // }
      
        // public function edit($id = null){
        //     if(!isset($id)) redirect('');

        //     $this->M_userrequest->edit($id);           
        //     $this->session->set_flashdata('edit_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>level user Berhasil diupdate</div>');
        //     redirect(site_url('manager/Manage_RequestUser/index'));  
        // }

        // public function delete($id = null){
        //     if (!isset($id)) redirect('');

        //     $this->m_user->delete($id);
        //     $this->session->set_flashdata('delete_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>User Berhasil dihapus</div>');
        //     redirect(site_url('manager/Manage_RequestUser/index'));
        // }

        public function accept($id = null){
            if (!isset($id)) redirect('');

            $this->M_userrequest->confirm($id);
            $this->M_userrequest->update_userAccept($id);
            redirect(site_url('manage_santri/index'));
        }

        public function reject($id = null){
            if (!isset($id)) redirect('');

            $this->M_userrequest->reject($id);
            redirect(site_url('Manage_requestuser/index'));
        }
    }
?>