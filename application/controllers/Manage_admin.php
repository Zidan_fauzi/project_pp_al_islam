<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_admin extends CI_Controller {

        function __construct(){

            parent::__construct();
            $this->load->model('M_admin');
            $this->load->model('M_santri');
            $this->load->model('M_dashboard');
            $this->load->helper('url', 'time_ago');
            $this->load->helper(array('string', 'text'));
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->library('pagination');
            if ($this->session->userdata('id_admin') == null) {
                redirect('Home');
            }
        }

        public function index()
        {
            $per_page = 1000;
            $page = 1000;
            $data['admin'] = $this->M_admin->getAll();   
            $data['data_admin'] = $this->M_dashboard->getUser();        

            $data['page'] = 'admin';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/admin/manage_admin', $data);
            $this->load->view('manager/footer');
        }

        public function daftar_admin()
        {
            $pengacak = "p3ng4c4k";
            $password = MD5($this->input->post('password'));
            $password = md5($pengacak . md5($password));

            $config['upload_path'] = './gambar_profile/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $nmfile = "file_".time();
            $config['file_name'] = $nmfile;
            $this->load->library('upload', $config);        
            if($this->upload->do_upload('gambar')){
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name = 'gambar_profile/'.$upload_data['file_name'];
            }

            $data = array(             
                'email' => $_POST['email'],
                'nama' => $_POST['nama'],
                'level' => 0,
                'created_at' => date("y-m-d H:i:s"),
                'updated_at' => date("y-m-d H:i:s"),
                'gambar' => $file_name,
                'password' => $password,                          
                'level'   => 1,
                'is_delete' => 0
            );
            $this->M_santri->daftarAdmin($data);
            redirect(site_url('Manage_admin/index'));
        }

        public function indexCreate()
        {
            $data["page"] = "admin";
            $data['data_admin'] = $this->M_dashboard->getUser(); 
            $this->load->view('manager/header', $data);
            $this->load->view('manager/admin/create_admin', $data);
            $this->load->view('manager/footer');           
        }

        public function delete($id)
        {
            if (!isset($id)) redirect('');
            if ($this->M_admin->delete($id)) {
                $data["admin"]= $this->M_admin->getByIdDelete($id);
                $this->session->set_flashdata('deleteadmin', '<div class="alert alert-danger fade show" role="alert">
                    Kamu Menghapus Admin Dengan Nama '.$data["admin"]->nama.'. Apakah Kamu Ingin Mengembalikan Admin Dengan Nama '.$data["admin"]->nama.' ? <a href="'.base_url('Manage_admin/restore/'.$id).'" class="alert-link">Kembalikan Admin <i class="fa fa-fw" aria-hidden="true" title="Copy to use rotate-left"></i></a>. 
                </div>');
                redirect(site_url('Manage_admin/index'));
            }
        }

        public function restore($id)
        {
            if ($this->M_admin->restore($id)) {
                $data["admin"]= $this->M_admin->getByIdDelete($id);
                $this->session->set_flashdata('restoreadmin', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Mengembalikan Admin Dengan Nama'.$data["admin"]->nama.'.
                </div>');
                redirect(site_url('Manage_admin/index'));
            }
        }

        public function checkDataByEmail(){
            $hasil = $this->M_admin->Check($_POST['email']);
            if ($hasil != null) {
                echo "Hasil";
            }else{
                echo "";
            }
        }
        
    }
?>