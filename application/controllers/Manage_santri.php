<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_santri extends CI_Controller {

      function __construct(){

            parent::__construct();

            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->load->model('M_santri');
            $this->load->library('session');
            $this->load->model('M_dashboard');
            $this->load->library('pagination');
            if ($this->session->userdata('id_admin') == null) {
                redirect('Home');
            }
        }

        public function index(){
            $data['santri'] = $this->M_santri->getAll();
            $data['page'] = 'santri';
            $data['pesan'] = '';
            $data['pesan_notfounddata'] = '';
            $data['data_admin'] = $this->M_dashboard->getUser();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/santri/manage_santri', $data);
            $this->load->view('manager/footer');
        }

        public function indexUpdate($id){
            $data['santri'] = $this->M_santri->getAllById($id);
            $data['page'] = 'santri';
            $data['pesan'] = '';
            $data['pesan_notfounddata'] = '';
            $data['data_admin'] = $this->M_dashboard->getUser();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/santri/update_santri', $data);
            $this->load->view('manager/footer');
        }

        public function edit($id){
            $config['upload_path'] = './gambar_santri/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $nmfile = "Foto_Profile_".time();
            $config['file_name'] = $nmfile;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('gambar_santri')){
                $config['upload_path'] = './gambar_santri/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name = 'gambar_santri/'.$upload_data['file_name'];
                $data = array(
                    'nama' => $_POST['nama'], 
                    'jenis_kelamin' => $_POST['JenisKelamin'],
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'tempat_lahir' => $_POST['tempat_lahir'],
                    'tanggal_lahir' => $_POST['tanggal_lahir'],
                    'alamat' => $_POST['alamat'],
                    'nama_orangtua' => $_POST['nama_ortu'],
                    'no_handphone' => $_POST['no_HP'],
                    'gambar_profile' => $file_name,
                    'is_delete' => 0
                );
                $this->M_santri->updateSantri($data, $id);
                $this->session->set_flashdata('create', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Artikel Berhasil diubah</div>');

                $this->index();
            }else{
                $data = array(
                    'nama' => $_POST['nama'], 
                    'jenis_kelamin' => $_POST['JenisKelamin'],
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'tempat_lahir' => $_POST['tempat_lahir'],
                    'tanggal_lahir' => $_POST['tanggal_lahir'],
                    'alamat' => $_POST['alamat'],
                    'nama_orangtua' => $_POST['nama_ortu'],
                    'no_handphone' => $_POST['no_HP'],
                    'is_delete' => 0
                );
                $this->M_santri->updateSantri($data, $id);
                $this->index();
            }
        }

        public function delete($id)
        {
            $data2 = array(
                'status' => 2
            );
            $this->M_santri->updateSantri($data2, $id);
            $this->M_santri->updateRequsetSantri($data2, $id);
            $this->index();
        }     
    }
?>