<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_artikel extends CI_Controller {

        function __construct(){

                parent::__construct();

                $this->load->model('M_artikel');
                $this->load->model('M_dashboard');
                $this->load->helper('url', 'time_ago');
                $this->load->helper(array('string', 'text'));
                $this->load->library('form_validation');
                $this->load->library('session');
                $this->load->library('pagination');
            if ($this->session->userdata('id_admin') == null) {
                redirect('Home');
            }
        }

        public function index()
        {
            $per_page = 1000;
            $page = 1000;
            $config['total_rows'] = $this->M_artikel->jumlah_data();
            $data['artikel'] = $this->M_artikel->getAll();   
            $data['data_admin'] = $this->M_dashboard->getUser();         

            // var_dump($data['data']);exit();

            $data['page'] = 'artikel';
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/manage_artikel', $data);
            $this->load->view('manager/footer');
        }

        public function detail($id)
        {
            $this->load->model('M_artikel');
            $data['artikel'] = $this->M_artikel->getArtikel($id);  
            $data["page"] = "artikel";
            $data["kategori"] = $this->M_artikel->get_kategori();
            $data['data_admin'] = $this->M_dashboard->getUser();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/detail_artikel', $data);
            $this->load->view('manager/footer'); 
        }

        public function indexCreate()
        {
            $data["page"] = "artikel";
            $data["kategori"] = $this->M_artikel->get_kategori();
            $data['data_admin'] = $this->M_dashboard->getUser();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/create_artikel', $data);
            $this->load->view('manager/footer');           
        }

        public function create()
        {
            $config['upload_path'] = './gambar/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $nmfile = "file_".time();
            $config['file_name'] = $nmfile;
            $this->load->library('upload', $config);        
            if($this->upload->do_upload('gambar')){
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name = 'gambar/'.$upload_data['file_name'];
                $data = array(
                    'nama_artikel' => $_POST["namaArtikel"], 
                    'id_kategori' => $_POST["kategori"],
                    'id_admin' => $this->session->userdata('id_admin'),
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'deskripsi' => $post["deskripsi"],
                    'slug' => $post["slug"],
                    'gambar' => $file_name,
                    'is_delete' => 0
                );
                $id = $this->M_artikel->uploadArticle($data);
                $this->session->set_flashdata('create', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Membuat Artikel Dengan Judul
                        <a href="'.base_url('Manage_artikel/detail/'.$this->db->insert_id()).'" class="alert-link">'.$_POST["namaArtikel"].'</a>. 
                    Cek Artikelmu.
                </div>');

                redirect(site_url('Manage_Artikel/index'));
                
            }else{

            }
        }

        public function indexUpdate($id)
        {
            $product = $this->M_artikel;
            $data["kategori"]= $product->get_kategori();
            $data["artikel"]= $product->getById($id);
            $data['data_admin'] = $this->M_dashboard->getUser();
            // print_r($data);
            // exit();
                     
            $data["page"] = "artikel";
            $this->load->view('manager/header', $data);
            $this->load->view('manager/artikel/update_artikel', $data);
            $this->load->view('manager/footer'); 
        }

        public function edit($id)
        {
            $product = $this->M_artikel;
            $gambar='';
            $config['upload_path'] = './gambar/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $nmfile = "file_".time();
            $config['file_name'] = $nmfile;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('gambar')){
            
                $upload_data = $this->upload->data();
                $file_name = 'gambar/'.$upload_data['file_name'];
                
            }else{
                echo $this->upload->display_errors('','');
            }
            
            $data["artikel"]= $product->getById($id);
            $product->updateArticle($id, $file_name);  
            
            $this->session->set_flashdata('edit', '<div class="alert alert-success fade show" role="alert">
                    Kamu Baru Saja Merubah Artikel Dengan Judul
                        <a href="'.base_url('Manage_artikel/detail/'.$id).'" class="alert-link">'.$_POST["namaArtikel"].'</a>. 
                    Cek Artikelmu.
                </div>');
            redirect(site_url('Manage_Artikel/index'));       
        }

        public function delete($id)
        {
            if (!isset($id)) redirect('');
            if ($this->M_artikel->delete($id)) {
                $data["artikel"]= $this->M_artikel->getByIdDelete($id);
                $this->session->set_flashdata('delete', '<div class="alert alert-danger fade show" role="alert">
                    Kamu Menghapus Artikel Dengan Judul '.$data["artikel"]->nama_artikel.'. Apakah Kamu Ingin Mengembalikan Artikelmu ? <a href="'.base_url('Manage_artikel/restore/'.$id).'" class="alert-link">Kembalikan Artikel <i class="fa fa-fw" aria-hidden="true" title="Copy to use rotate-left"></i></a>. 
                </div>');
                redirect(site_url('Manage_Artikel/index'));
            }
        }

        public function restore($id)
        {
            if ($this->M_artikel->restore($id)) {
                $data["artikel"]= $this->M_artikel->getByIdDelete($id);
                $this->session->set_flashdata('create', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Mengembalikan Artikel Dengan Judul
                        <a href="'.base_url('Manage_artikel/detail/'.$id).'" class="alert-link">'.$data["artikel"]->nama_artikel.'</a>. 
                    Cek Artikelmu.
                </div>');
                redirect(site_url('Manage_Artikel/index'));
            }
        }
        
    }
?>