<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

      function __construct(){

            parent::__construct();
            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->load->model('M_santri');
            $this->load->library('session');
            $this->load->library('pagination');
        }

        public function index(){
            $this->load->view('pendaftaran');
        }

        public function daftar()
        {
            $config['upload_path'] = './gambar_santri/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $nmfile = "Foto_Profile_".time();
            $config['file_name'] = $nmfile;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('gambar_santri')){
                $config['upload_path'] = './gambar_santri/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name = 'gambar_santri/'.$upload_data['file_name'];
                $data = array(
                    'nama' => $_POST['nama'], 
                    'jenis_kelamin' => $_POST['JenisKelamin'],
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'tempat_lahir' => $_POST['tempat_lahir'],
                    'tanggal_lahir' => $_POST['tanggal_lahir'],
                    'alamat' => $_POST['alamat'],
                    'nama_orangtua' => $_POST['nama_ortu'],
                    'no_handphone' => $_POST['no_HP'],
                    'gambar_profile' => $file_name,
                    'is_delete' => 0
                );
                $this->M_santri->uploadSantri($data);
                $data = array(
                    'created_at' => date("y-m-d H:i:s"),
                    'updated_at' => date("y-m-d H:i:s"),
                    'id_santri' => $this->db->insert_id(),
                    'status' => 0
                );
                $this->M_santri->insertRequest($data);
                $this->session->set_flashdata('create', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Artikel Berhasil ditambahkan</div>');

                redirect('');
            }else{
                $this->index();
            }
        }

        public function index_daftar_admin()
        {
            $this->load->view('daftar');
        }

        public function daftar_admin()
        {
            $pengacak = "p3ng4c4k";
            $password = MD5($this->input->post('password'));
            $password = md5($pengacak . md5($password));

            $config['upload_path'] = './gambar_profile/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $nmfile = "file_".time();
            $config['file_name'] = $nmfile;
            $this->load->library('upload', $config);        
            if($this->upload->do_upload('gambar')){
                $post = $this->input->post();
                $upload_data = $this->upload->data();
                $file_name = 'gambar_profile/'.$upload_data['file_name'];
            }

            $data = array(             
                'email' => $_POST['email'],
                'nama' => $_POST['nama'],
                'level' => 0,
                'created_at' => date("y-m-d H:i:s"),
                'updated_at' => date("y-m-d H:i:s"),
                'gambar' => $file_name,
                'password' => $password,                          
                'level'   => 1,
                'is_delete' => 0
            );
            $this->M_santri->daftarAdmin($data);
            redirect('Login');
        }

        public function success_daftar(){
            $this->load->view('success_daftar');
        }
    }
?>