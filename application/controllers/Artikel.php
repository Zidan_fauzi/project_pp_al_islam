<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {
	function __construct(){

	    parent::__construct();

	    $this->load->model('M_artikel_frontpage');
	}
	public function baca($slug)
	{
		$data['more_artikel'] = $this->M_artikel_frontpage->more_artikel();
		$data['isi_artikel'] = $this->M_artikel_frontpage->get_artikel($slug);
		if ($data['isi_artikel'][0]->is_delete == 1) {
			redirect('Home');
		}
		$this->load->view('frontpage/v_artikel',$data);
	}
}
