<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_kategori extends CI_Controller {

        function __construct(){

            parent::__construct();

            $this->load->model('M_kategori');
            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->load->model('M_dashboard');
            $this->load->library('session');
            $this->load->library('pagination');
            if ($this->session->userdata('id_admin') == null) {
                redirect('Home');
            }
        }

        public function index()
        {
            $data['kategori'] = $this->M_kategori->getAll();
            $data['pesan'] = '';
            $data['pesan_notfounddata'] = '';
            $data['page'] = 'kategori';

            $data['data_admin'] = $this->M_dashboard->getUser();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/kategori/manage_kategori', $data);
            $this->load->view('manager/footer');
        }

        public function indexUpdate($id)
        {
            $data['kategori'] = $this->M_kategori->getById($id);
            $data["page"] = "kategori";
            $data['data_admin'] = $this->M_dashboard->getUser();
            $this->load->view('manager/header', $data);
            $this->load->view('manager/kategori/update_kategori', $data);
            $this->load->view('manager/footer'); 
        }

        public function edit($id)
        {
            $this->M_kategori->edit($id);
            $this->session->set_flashdata('create', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Membuat Kategori Dengan Nama
                        <a href="'.base_url('Manage_kategori/indexUpdate/'.$id).'" class="alert-link">'.$_POST["kategori"].'</a>. 
                    Cek Kategorimu.
                </div>');

            redirect(site_url('Manage_Kategori/index'));
        }   

        public function create()
        {
            $this->M_kategori->upload();
            $this->session->set_flashdata('create', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Membuat Kategori Dengan Nama
                        <a href="'.base_url('Manage_kategori/indexUpdate/'.$this->db->insert_id()).'" class="alert-link">'.$_POST["kategori"].'</a>. 
                    Cek Kategorimu.
                </div>');
            redirect(site_url('manage_kategori'));
        }


        public function delete($id)
        {
            if (!isset($id)) redirect('');
            $data["kategori"]= $this->M_kategori->getById($id);
            if ($this->M_kategori->delete($id)) { 
                $this->M_kategori->deleteArtikelByKategori($id);               
                $this->session->set_flashdata('create', '<div class="alert alert-danger fade show" role="alert">
                    Kamu Menghapus Kategori Dengan Nama'.$data["kategori"]->kategori.'. Apakah Kamu Ingin Mengembalikan Kategori Beserta Artikel yang Sudah Terhapus ? <a href="'.base_url('Manage_kategori/restore/'.$id).'" class="alert-link">Kembalikan Kategori dan Artikel <i class="fa fa-fw" aria-hidden="true" title="Copy to use rotate-left"></i></a>.
                </div>');
            }
           redirect(site_url('Manage_Kategori/index'));   
        }

        public function restore($id)
        {
            if (!isset($id)) redirect('');
            $data["kategori"]= $this->M_kategori->getById($id);
            if ($this->M_kategori->restore($id)) { 
                $this->M_kategori->restoreKategoriById($id); 
                $this->session->set_flashdata('restore', '<div class="alert alert-success fade show" role="alert">
                    Kamu Berhasil Mengembalikan Kategori Dengan Nama
                        <a href="'.base_url('Manage_kategori/indexUpdate/'.$id).'" class="alert-link">'.$data["kategori"]->kategori.'</a>. 
                    Cek Kategorimu.
                </div>');
                redirect(site_url('Manage_kategori/index'));
            }
        }
    }
?>