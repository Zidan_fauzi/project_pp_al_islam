-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2020 at 02:14 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pondokpesantren`
--

-- --------------------------------------------------------

--
-- Table structure for table `santri_request`
--

CREATE TABLE `santri_request` (
  `id` bigint(4) NOT NULL,
  `id_santri` bigint(4) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `santri_request`
--

INSERT INTO `santri_request` (`id`, `id_santri`, `status`, `created_at`, `updated_at`, `is_delete`) VALUES
(1, 1, 1, '2020-01-12 11:39:05', '2020-01-12 12:03:14', 0),
(2, 2, 1, '2020-01-12 11:45:15', '2020-01-12 11:45:42', 0),
(3, 3, 0, '2020-01-12 16:18:17', '2020-01-12 16:18:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` bigint(4) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL,
  `cookie` text NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `is_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `email`, `nama`, `password`, `gambar`, `level`, `cookie`, `updated_at`, `created_at`, `is_delete`) VALUES
(1, 'admin@gmail.com', 'admin', '942ae13e7ef81210c3675d5a3358eca0', 'gambar_profile/file_1578804079.jpg', '0', '', '2020-01-12 19:34:24', '2020-01-12 00:00:00', 0),
(2, 'faras@gmail.com', 'faras', 'a9e866f354b8388154d81bcae5804fbb', 'gambar_profile/file_1578804813.jpg', '1', '', '2020-01-12 11:54:26', '2020-01-12 11:53:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE `tb_artikel` (
  `id_artikel` bigint(4) NOT NULL,
  `nama_artikel` varchar(200) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `id_kategori` bigint(4) NOT NULL,
  `id_admin` bigint(4) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(500) NOT NULL,
  `video` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `is_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_artikel`
--

INSERT INTO `tb_artikel` (`id_artikel`, `nama_artikel`, `slug`, `id_kategori`, `id_admin`, `deskripsi`, `gambar`, `video`, `created_at`, `updated_at`, `is_delete`) VALUES
(1, 'pendaftaran santri baru telah di buka', 'pendaftaran-santri-baru-telah-di-buka', 1, 1, '<p>pondok pesantren al islam telah membuka pendaftaran santri baru tahun 2020</p>\r\n', 'gambar/file_1578804665.jpg', '', '2020-01-12 11:51:05', '2020-01-12 11:51:05', 0),
(2, 'islam itu indah', 'islam-itu-indah', 1, 2, '<p>islam mengajarkan kita harus saling mengasihi</p>\r\n\r\n<p>Link : <a href=\"https://www.youtube.com/watch?v=khO2xFAtLPg\">https://www.youtube.com/watch?v=khO2xFAtLPg</a></p>\r\n', 'gambar/file_1578805008.jpg', '', '2020-01-12 11:56:48', '2020-01-12 12:08:40', 0),
(3, 'Daftar Pengajian Rutin - Yang Dilaksanakan di pondok Pesantren Al-Islam', 'daftar-pengajian-rutin-yang-dilaksanakan-di-pondok-pesantren-al-islam', 1, 1, '<p><strong>Lorem Ipsum</strong> adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</p>\r\n', 'gambar/file_1578818648.jpg', '', '2020-01-12 15:43:50', '2020-01-12 15:44:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` bigint(4) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `is_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `kategori`, `slug`, `updated_at`, `created_at`, `is_delete`) VALUES
(1, 'pendidikan islam', 'pendidikan-islam', '2020-01-12 11:48:52', '2020-01-12 11:48:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_santri`
--

CREATE TABLE `tb_santri` (
  `id_santri` int(4) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis_kelamin` int(2) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `nama_orangtua` varchar(50) NOT NULL,
  `no_handphone` varchar(15) NOT NULL,
  `gambar_profile` varchar(100) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_santri`
--

INSERT INTO `tb_santri` (`id_santri`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `nama_orangtua`, `no_handphone`, `gambar_profile`, `status`, `created_at`, `updated_at`, `is_delete`) VALUES
(1, 'Mohammad Zidan Fauzi', 1, 'Ma', '2002-02-08', 'Jl Diponegoro', 'Imam Fauzi', '08', 'gambar_santri/Foto_Profile_1578803945.jpg', 1, '2020-01-12 11:39:05', '2020-01-12 12:03:14', 0),
(2, 'oky lukmsn', 1, 'malang', '2020-11-01', 'jl kalipare ', 'nur fadiah', '082121111111', 'gambar_santri/Foto_Profile_1578804465.jpg', 1, '2020-01-12 11:47:45', '2020-01-12 11:47:45', 0),
(3, 'Mohammad Zidan Fauzi', 1, 'Malang', '2002-02-28', 'Jl Diponegoro', 'Imam Fauzi', '082121111111', '../gambar_santri/Foto_Profile_1578820697.jpg', 0, '2020-01-12 16:18:17', '2020-01-12 16:18:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ustadz`
--

CREATE TABLE `tb_ustadz` (
  `id_ustadz` int(4) NOT NULL,
  `nama_ustadz` varchar(100) NOT NULL,
  `quotes` text NOT NULL,
  `foto_ustadz` varchar(50) NOT NULL,
  `status` int(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_delete` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ustadz`
--

INSERT INTO `tb_ustadz` (`id_ustadz`, `nama_ustadz`, `quotes`, `foto_ustadz`, `status`, `created_at`, `updated_at`, `is_delete`) VALUES
(1, 'Khoiruman', 'Islam itu Indah ', '', 1, '2020-01-05 00:00:00', '2020-01-10 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `santri_request`
--
ALTER TABLE `santri_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_santri`
--
ALTER TABLE `tb_santri`
  ADD PRIMARY KEY (`id_santri`);

--
-- Indexes for table `tb_ustadz`
--
ALTER TABLE `tb_ustadz`
  ADD PRIMARY KEY (`id_ustadz`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `santri_request`
--
ALTER TABLE `santri_request`
  MODIFY `id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  MODIFY `id_artikel` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_santri`
--
ALTER TABLE `tb_santri`
  MODIFY `id_santri` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_ustadz`
--
ALTER TABLE `tb_ustadz`
  MODIFY `id_ustadz` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
